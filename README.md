This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

This project is the front-end code for the Mobile Pervasive Computing Course Final Project. It uses:
- ReactJS 16.9.0 as its main **front-end framework**
- React Router 5.1.1 for **navigation**
- Material UI 4.4.2 as its **UI Framework**
- Paho MQTT JS 2.0.5 for **MQTT over WS/WSS client**
- Axios for **handling API Requests**
- Date-Fns as **time-handling library**
- ES6 JavaScript

Each of the component's passed props, methods, and internal states are written in the respective file

## Folder Structure

src  
|- components  
|        |- Components_Dashboard  
|        |       |- BigDisplay.js  
|        |       |- Dialogs.js  
|        |       |- LightSettings.js  
|        |       |- PhotoCard.js  
|        |       |- RecipeCard.js  
|        |       |- SmallDisplay.js  
|        |       |- StatusBar.js  
|        |  
|        |- Components_Web  
|                |- RecipeDetails.js  
|                |- RecipeSearch.js  
|  
|- Logic  
|        |- Logic_Dashboard  
|        |       |- DevicesLogic.js  
|        |  
|        |- Logic_Web  
|        |       |- AuthLogic.js  
|        |       |- RecipesLogic.js  
|        |  
|        |- logError.js  
|  
|- Pages  
|        |- Pages_Dashboard  
|        |       |- DevicesPage.js  
|        |       |- MonitorPage.js  
|        |       |- PhotosPage.js  
|        |       |- RecordsPage.js  
|        |  
|        |- Pages_Web  
|                |- LoginPage.js  
|                |- ProfilePage.js  
|                |- RecipesPage.js  
|                |- RegisterPage.js  


## DOM Structure

App.js  
|- LoginPage.js  
|- RegisterPage.js  
|- Home.js  
|       |- ProfilePage.js (still unused)  
|       |- RecipesPage.js  
|               |- RecipeSearch.js  
|               |- RecipeDetails.js  
|  
|- Dashboard.js  
        |- StatusBar.js  
        |- Dialogs.js  
        |- RecordsPage.js  
        |- DevicesPage.js  
        |- PhotosPage.js  
        |- MonitorPage.js  
                 |- BigDisplay.js  
                 |- SmallDisplay.js  
                 |- LightSettings.js  
                 |- PhotoCard.js  

