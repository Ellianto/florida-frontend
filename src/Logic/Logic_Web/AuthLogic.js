import axios from 'axios';
import logError from '../logError';

// const domain = 'http://192.168.1.17:8000';
const domain = 'https://rocky-gorge-67846.herokuapp.com';
const version = 'v1';
const baseUrl = domain + '/api/' + version;

// Mutation/Side Effect Functions

export async function doLogin(uname, passwd){
    const apiPath = '/authentication/person';
    const params = {
        username : uname,
        password : passwd,
    };

    try {
        const response = await axios.post(baseUrl + apiPath, params);
        localStorage.setItem('AuthToken', response.data.token);
        return true;
    } catch (error) {
        logError(error);
        return false;
    }
}

export async function doRegister(username, password, firstName, lastName, countryID, cityID){
    const apiPath = '/authentication/register/person';
    const params = {
        username : username,
        password : password,
        first_name : firstName,
        last_name : lastName,
        country : countryID,
        city : cityID,
    }

    try {
        const response = await axios.post(baseUrl + apiPath, params);
        return response.data.status === 'success';
    } catch (error) {
        logError(error);
        return false;
    }
}

export async function checkToken() {
    const authToken = localStorage.getItem('AuthToken');

    if (authToken === null) {
        return false;
    }

    const apiPath = '/authentication/refresh';

    const params = {
        token: authToken,
    }

    try {
        const response = await axios.post(baseUrl + apiPath, params);
        localStorage.setItem('AuthToken', response.data.token);
        return true;
    } catch (error) {
        logError(error);
        return false;
    }
}

// Fetch/Query Functions

export async function getCountries(){
    const apiPath = '/country';
    
    try {
        const response = await axios.get(baseUrl + apiPath);        
        const countriesArr = response.data;

        return countriesArr;
    } catch (error) {
        logError(error);
        return [];
    }
}

export async function getCities(countryID){
    const apiPath = '/city/' + countryID;

    try {
        const response = await axios.get(baseUrl + apiPath);

        const citiesArr = response.data;

        return citiesArr;
    } catch (error) {
        logError(error);
        return [];
    }
}

export async function getUserData(){
    if (!checkToken()) {
        return null;
    }

    const authToken = localStorage.getItem('AuthToken');

    if (authToken === null) {
        return null;
    }

    const apiPath = '/user/profile';
    const headers = {
        'Authorization': 'JWT ' + authToken,
    };

    try {
        const response = await axios.get(baseUrl + apiPath, {headers: headers});
        
        if(response.status >= 200 && response.status < 400 && response.data.status === 'success'){
            return response.data;
        } else return null;
    } catch (error) {
        logError(error);
        return null;
    }
}