import axios from 'axios';
import logError from '../logError';
import { checkToken } from '../Logic_Web/AuthLogic';

const domain = 'https://rocky-gorge-67846.herokuapp.com';
const version = 'v1';
const baseUrl = domain + '/api/' + version;

// Fetch/Query Functions

export async function searchRecipe(searchQuery){
    if (!checkToken()) {
        return [];
    }

    const authToken = localStorage.getItem('AuthToken');

    if (authToken === null) {
        return [];
    }    

    const apiPath = '/recipe';
    const params = {
        query: searchQuery,
    }

    const headers = {
        'Authorization': 'JWT ' + authToken,
    }

    try {
        const response = await axios.get(baseUrl + apiPath, {
            params: params,
            headers: headers,
        });

        if(response.status >= 200 && response.status < 400){
            const recipeArr = response.data;
            
            return recipeArr.map(recipeObj => ({
                recipeID: recipeObj.id,
                recipeName: recipeObj.name,
                recipeStart: recipeObj.created,
                recipeDesc: recipeObj.description,
                recipeOwner: recipeObj.owner_user.first_name + ' ' + recipeObj.owner_user.last_name,
            })) ;
        } else return [];
    } catch (error) {
        logError(error);
        return [];
    }
}

export async function getRecipeDetails(recipeID){
    if (!checkToken()) {
        return null;
    }

    const authToken = localStorage.getItem('AuthToken');

    if (authToken === null) {
        return null;
    }

    const headers = {
        'Authorization': 'JWT ' + authToken,
    }

    const apiPath = '/recipe';
    const params = {
        query: '',
    }

    try {
        const response = await axios.get(baseUrl + apiPath, {
            headers: headers, 
            params: params,
        });

        if(response.status >= 200 && response.status < 400){
            const recipeArr = response.data;

            const recipeObj = recipeArr.find((recipe) => {
                return recipe.id === parseInt(recipeID,10);
            });

            return {
                recipeID: recipeObj.id,
                recipeName: recipeObj.name,
                recipeStart: (new Date(recipeObj.created)).valueOf(),
                recipeEnd: (new Date(recipeObj.modified)).valueOf(),
                recipeDesc: recipeObj.description,
                recipeOwner: recipeObj.owner_user.first_name + ' ' + recipeObj.owner_user.last_name,
            };
        } else return null;
    } catch (error) {
        logError(error);
        return null;
    }
}