import axios from 'axios';
import logError from '../logError';
import {checkToken} from '../Logic_Web/AuthLogic';

// const domain = 'http://192.168.1.17:8000';
const domain = 'https://rocky-gorge-67846.herokuapp.com';
const version = 'v1';
const baseUrl = domain + '/api/' + version;

// Mutation/Side Effect Functions

export async function createDevice(deviceName) {
    if(!checkToken()){
        return null;
    }

    const authToken = localStorage.getItem('AuthToken');

    if (authToken === null) {
        return null;
    }

    const apiPath = '/authentication/register/device';

    const params = {
        device_name: deviceName,
    };

    const headers = {
        'Authorization': 'JWT ' + authToken,
    };

    try {
        const response = await axios.post(baseUrl + apiPath, params, { headers: headers });
        if (response.data.status === 'success') {
            return response.data.serial_number;
        } else {
            return null;
        }
    } catch (error) {
        logError(error);
        return null;
    }
}

export async function updateConfig(newConfig, serialNumber) {
    if (!checkToken()) {
        return false;
    }

    const authToken = localStorage.getItem('AuthToken');

    if (authToken === null) {
        return false;
    }

    const apiPath = '/foodcomputer/configuration/';
    const body = newConfig;

    const params = {
        serialNumber: serialNumber,
    }

    const headers = {
        'Authorization': 'JWT ' + authToken,
    }

    try {
        const response = await axios.patch(baseUrl + apiPath, body, { headers: headers, params: params })
        if (response.data.status === 'success') {
            return true;
        } else {
            return false;
        }
    } catch (error) {
        logError(error);
        return false;
    }
}

export async function createRecipe(recipeName, recipeDesc, serialNumber){
    if (!checkToken()) {
        return false;
    }

    const authToken = localStorage.getItem('AuthToken');

    if (authToken === null) {
        return false;
    }

    const apiPath = '/recipe?serialNumber=' + serialNumber;

    const headers = {
        'Authorization': 'JWT ' + authToken,
    }

    const params = {
        recipe_name: recipeName,
        description: recipeDesc,
    }

    try {
        const response = await axios.post(baseUrl + apiPath, params, {headers: headers});

        if(response.status >= 200 && response.status < 400 && response.data.status === 'success'){
            return true;
        } else {
            return false;
        }
    } catch (error) {
        logError(error);
        return false;
    }
}

export async function endRecipe(recipeID){
    if (!checkToken()) {
        return null;
    }

    const authToken = localStorage.getItem('AuthToken');

    if (authToken === null) {
        return false;
    }

    const apiPath = '/recipe/finish/' + recipeID;

    const headers = {
        'Authorization': 'JWT ' + authToken,
    }

    try {
        const response = await axios.put(baseUrl + apiPath, null, {headers: headers});

        if(response.status >= 200 && response.status < 400){
            return response.data.status === 'success';
        } else return false;
    } catch (error) {
        logError(error);
        return false;
    }
}

// Query/Fetching Functions

export async function fetchMeasurements(deviceId){
    if (!checkToken()) {
        return null;
    }

    const authToken = localStorage.getItem('AuthToken');

    if(authToken === null){
        return null;
    }

    const apiPath = '/foodcomputer/monitor/' + deviceId + '/';

    const headers = {
        'Authorization' : 'JWT ' + authToken,
    }

    try {
        const response = await axios.get(baseUrl + apiPath, {headers: headers});

        if(response.status === 200){
            return response.data.data;
        } else {
            return null;
        }
    } catch (error) {
        logError(error);
        return null;
    }
}

export async function fetchPhotos(serialNumber) {
    if (!checkToken()) {
        return null;
    }

    const authToken = localStorage.getItem('AuthToken');

    if (authToken === null) {
        return null;
    }

    const apiPath = '/foodcomputer/image/';
    const params = {
        serialNumber: serialNumber,
    }

    const headers = {
        'Authorization': 'JWT ' + authToken,
    }

    try {
        const response = await axios.get(baseUrl + apiPath, {
            params: params,
            headers: headers,
        });

        if (response.status >= 200 && response.status < 400) {
            if (response.data.length > 0) {
                return response.data.map(photo => ({
                    id: photo.id,
                    timestamp: (new Date(photo.created)),
                    image: domain + photo.image,
                })).sort((a, b) => { return (new Date(a.created)).valueOf() - (new Date(b.created)).valueOf()})
            } else {
                return [];
            }
        } else {
            return null;
        }
    } catch (error) {
        logError(error);
        return null;
    }
}

export async function fetchLatestPhoto(serialNumber){
    if (!checkToken()) {
        return null;
    }
    
    const authToken = localStorage.getItem('AuthToken');

    if (authToken === null) {
        return null;
    }
    
    const apiPath = '/foodcomputer/image/';
    const params = {
        serialNumber : serialNumber,
    }

    const headers = {
        'Authorization': 'JWT ' + authToken,
    }

    try {
        const response = await axios.get(baseUrl + apiPath, {
            params: params,
            headers: headers,
        });

        if(response.status >= 200 && response.status < 400){
            if(response.data.length > 0){
                return domain + response.data.pop().image;
            } else {
                return '/img/logo192.png';
            }
        } else {
            return null;
        }
    } catch (error) {
        logError(error);
        return null;
    }
}

export async function fetchRecords(recipeID){
    if (!checkToken()) {
        return null;
    }

    const authToken = localStorage.getItem('AuthToken');

    if (authToken === null) {
        return null;
    }

    const apiPath = '/recipe/detail/' + recipeID;

    const headers = {
        'Authorization': 'JWT ' + authToken,
    }
    
    try {
        const response =  await axios.get(baseUrl + apiPath,{headers: headers});
        if(response.status >= 200 && response.status < 400){
            return response.data.map(record => ({
                id: record.id,
                age: record.TimestampDelta,
                illuminance: record.data.lux,
                humidity: record.data.humidity,
                temperature: record.data.temperature,
                tdsMeasure : record.data.tds,
                tdsSet : record.tds_value,
                nutrientsA: record.nutrisiA,
                nutrientsB: record.nutrisiB,
                colorOn: record.lamp_uv_enable,
                whiteOn: record.lamp_grow_enable,
            })).sort((a,b) => {return a.timestamp - b.timestamp});
        } else {
            return null;
        }
    } catch (error) {
        logError(error);
        return null;
    }
}

export async function fetchDeviceList() {
    if (!checkToken()) {
        return null;
    }

    const authToken = localStorage.getItem('AuthToken');

    if (authToken === null) {
        return null;
    }

    const apiPath = '/foodcomputer/';

    const headers = {
        'Authorization': 'JWT ' + authToken,
    }

    try {
        const response = await axios.get(baseUrl + apiPath, { headers: headers });

        if (response.status >= 200 && response.status < 400) {
            return response.data.map(val => ({
                deviceID: val.id,
                serialNumber: val.serial_number,
                deviceName: val.device_name,
                imageLink: '/img/logo192.png',
                recipeID: val.main_recipe === null ? null : val.main_recipe.id,
                recipeName: val.main_recipe === null ? null : val.main_recipe.name,
                recipeStart: val.main_recipe === null ? null : (new Date(val.main_recipe.created)).valueOf(),
            }));
        } else return null;
    } catch (error) {
        logError(error);
        return null;
    }
}

export async function fetchConfiguration(serialNumber) {
    if (!checkToken()) {
        return null;
    }

    const authToken = localStorage.getItem('AuthToken');

    if (authToken === null) {
        return null;
    }

    const apiPath = '/foodcomputer/configuration/';

    const headers = {
        'Authorization': 'JWT ' + authToken,
    }

    const params = {
        serialNumber: serialNumber,
    }

    try {
        const response = await axios.get(baseUrl + apiPath, {
            params: params,
            headers: headers,
        });

        if (response.status === 200) {
            return response.data;
        } else {
            return null;
        }
    } catch (error) {
        logError(error);
        return null;
    }
}
