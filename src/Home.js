import { Button, Typography, AppBar, Toolbar, IconButton, Box, Container, Grid } from '@material-ui/core';
import React from 'react';
import { Route, Link } from 'react-router-dom';
import RecipesPage from './Pages/Pages_Web/RecipesPage';
import ProfilePage from './Pages/Pages_Web/ProfilePage';

/**
 * !Props
 * * Variables
 * userToken   : String, contains user's refresh token
 * 
 * * Methods
 * openSnackBar: HOC Methods to display a message via SnackBar
 * startLoading: HOC Methods to display LoadingOverlay
 * stopLoading : HOC Methods to hide LoadingOverlay
 * setUser     : HOC Methods to logically "log in"
 * logOut      : HOC Methods to logically "log out"
 * 
 */

function CustomButton(props) {
    return (
        props.to === null ? 
            <Button style={{ color: 'white' }} onClick={props.onClick}>
                {props.text}
            </Button>
            :
            <Link to={props.to} style={{textDecoration: 'none'}}>
                <Button style={{ color: 'white' }} onClick={props.onClick}>
                    {props.text}
                </Button>
            </Link>
    )
}
class Home extends React.Component {
    render() {
        return (
            <div id="home-page" style={{height:'100vh', background: 'url(/img/home-banner-bg.png) no-repeat', backgroundSize: 'cover' , backgroundPosition: 'center center' }}> 
                <Box ml={8} mr={8}>
                    <AppBar position='static' style={{ backgroundColor: 'rgba(0,0,0,0)' }} elevation={0}>
                        <Toolbar>
                            <Box style={{ flexGrow: '1' }}>
                                <IconButton edge='start' size='medium' onClick={() => { this.props.history.push('/') }}>
                                    <img src="/img/logo.png" alt="" />
                                </IconButton>
                            </Box>
                            <CustomButton text='Recipes' to='/recipes' />
                            {/* <CustomButton text='Hardware' to='/florida_hardware' /> */}
                            {
                                this.props.userToken === null ?
                                    <React.Fragment>
                                        <CustomButton text='Login' to='/login' />
                                        <CustomButton text='Sign Up' to='/signup' />
                                    </React.Fragment>
                                    :
                                    <React.Fragment>
                                        <CustomButton text='Profile' to='/profile' />
                                        <CustomButton text='Dashboard' to='/dashboard' />
                                        <CustomButton text='Log Out' to='/' onClick={this.props.logOut} />
                                    </React.Fragment>
                            }
                        </Toolbar>
                    </AppBar>
                    <Box mt={8}>
                        <Route exact path='/'>
                            <Container maxWidth="md">
                                <Grid container direction='row' alignItems='center' justify='space-between'>
                                    <Grid item xs={12} md={6}>
                                        <img src="img/header-img.png" alt="" style={{ maxWidth: '100%', height: 'auto' }} />
                                    </Grid>
                                    <Grid item xs={12} md={1} />
                                    <Grid item xs={12} md={5} style={{ color: 'white' }}>
                                        <Typography variant='h3' style={{ fontFamily: 'Playfair Display' }}>
                                            Flora Laboratory Open for
                                            Real Ingenious Deep Analysis
                                        </Typography>
                                        <Typography variant='h6' style={{ fontFamily: 'Playfair Display' }}>
                                            Creating an easy-to-use and community-based
                                                gardening platform to experiment
                                                and be creative in agriculture.
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Container>
                        </Route>
                        <Route exact path={['/recipes', '/recipes/:recipeID']} render={props => <RecipesPage {...props} startLoading={this.props.startLoading} stopLoading={this.props.stopLoading} openSnackBar={this.props.openSnackBar}/>} />
                        <Route exact path='/profile'>
                            <ProfilePage />
                        </Route>
                    </Box>
                </Box>
            </div>
        );
    }
}

export default Home;