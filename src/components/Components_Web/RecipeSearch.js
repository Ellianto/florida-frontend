import { Avatar, Card, CardContent, CardHeader, Checkbox, FormControl, FormControlLabel, FormGroup, FormLabel, Grid, IconButton, InputAdornment, TextField, Typography } from '@material-ui/core';
import Search from '@material-ui/icons/Search';
import React from 'react';

/**
 * !Props
 * * Variables
 * recipeList   : List of recipe based on the search result
 * 
 * * Methods
 * openSnackBar : HOC Method for displaying message in the snack bar`
 * doSearch     : HOC Method to send search query to API
 * handleRecipeCardClick: Method to redirect to RecipeDetails Page
 * 
 * !States:
 * searchQuery  : String, represents user input in the search field
 * label1,2,3   : For future use with filters and checkboxes
 */

function RecipeCard(props) {
    return (
        <Grid item container xs={12} direction='column'>
            <Card style={{ display: 'flex' }} onClick={() => { props.onClick(props.recipe.recipeID) }} elevation={3}>
                <Grid item xs={3}>
                    <img src={props.recipe.recipeImage} alt={props.recipe.recipeName + ' by ' + props.recipe.recipeOwner} style={{ objectFit: 'scale-down', marginLeft: '32px' }} />
                </Grid>
                <Grid item xs={9} container direction='row' alignItems='flex-start' justify='space-evenly'>
                    <Grid item xs={12}>
                        <CardContent>
                            <Typography variant='h6'>
                                {
                                    props.recipe.recipeName.length <= 100 ? props.recipe.recipeName : 
                                    props.recipe.recipeName.substring(0, 95) + '...'
                                }
                            </Typography>
                            <Typography variant='body2'>
                                {
                                    props.recipe.recipeDesc.length <= 250 ? props.recipe.recipeDesc :
                                    props.recipe.recipeDesc.substring(0,245) + '...'
                                }
                            </Typography>
                        </CardContent>
                    </Grid>
                    <Grid item xs={12}>
                        <CardHeader
                            title={props.recipe.recipeOwner}
                            avatar={
                                <Avatar style={{ backgroundColor: 'purple' }}>
                                    {props.recipe.recipeOwner.split(' ', 2).map(name => { return name.slice(0, 1) })}
                                </Avatar>
                            }
                        />
                    </Grid>
                </Grid>
            </Card>
        </Grid>
    )
}

class RecipeSearch extends React.Component {
    constructor(props){
        super(props);

        this.handleCheckBoxChange = this.handleCheckBoxChange.bind(this);
        this.handleSearchQueryChange = this.handleSearchQueryChange.bind(this);
        this.handleRecipeCardClick = this.handleRecipeCardClick.bind(this);

        this.state = {
            searchQuery: '',
            label1: false,
            label2: false,
            label3: false,
        }
    }

    handleSearchQueryChange(e) {
        this.setState({
            [e.target.name]: e.target.value,
        });
    }

    handleCheckBoxChange(e) {
        this.setState({
            [e.target.name]: e.target.checked,
        });
    }

    handleRecipeCardClick(recipeID) {
        this.props.history.push(this.props.match.path + '/' + recipeID);
    }

    render() {
        return (
            <Grid container spacing={2} direction='row' style={{ color: 'white'}}>
                <Grid item xs={12} md={2}>
                    <FormControl component='fieldset'>
                        <FormLabel component='legend' style={{color: 'white'}}> Filters </FormLabel>
                        <FormGroup>
                            <FormControlLabel
                                label='Label 1'
                                control={<Checkbox name='label1' checked={this.state.label1} onChange={this.handleCheckBoxChange} style={{ color: 'white' }}/>}
                            />
                            <FormControlLabel
                                label='Label 2'
                                control={<Checkbox name='label2' checked={this.state.label2} onChange={this.handleCheckBoxChange} style={{ color: 'white' }}/>}
                            />
                            <FormControlLabel
                                label='Label 3'
                                control={<Checkbox name='label3' checked={this.state.label3} onChange={this.handleCheckBoxChange} style={{ color: 'white' }}/>}
                            />
                        </FormGroup>
                    </FormControl>
                </Grid>
                <Grid item xs={12} md={10} container direction='row' alignItems='flex-start' justify='space-between' spacing={4}>
                    <Grid item xs={12}>
                        <TextField fullWidth
                            variant='outlined'
                            type='text'
                            label='Search Field'
                            helperText='Search for relevant recipe names here'
                            name='searchQuery'
                            margin='normal'
                            value={this.state.searchQuery}
                            onChange={this.handleSearchQueryChange}
                            InputLabelProps={{
                                style:{
                                    color: 'white',
                                },
                            }}
                            FormHelperTextProps={{
                                style: {
                                    color: 'white',
                                },
                            }}
                            InputProps={{
                                style:{
                                    color: 'white',
                                    borderColor:'white',
                                },
                                endAdornment: (
                                    <InputAdornment position='end'>
                                        <IconButton edge='end' style={{ color: 'white' }} onClick={async () => {await this.props.doSearch(this.state.searchQuery)}} >
                                            <Search fontSize='large' />
                                        </IconButton>
                                    </InputAdornment>
                                ),
                            }}
                        />
                    </Grid>
                    {
                        this.props.recipeList.length === 0 ?
                            null :
                            <Grid item xs={12} container direction='row' alignItems='flex-start' justify='space-around' spacing={2}>
                                {this.props.recipeList.map(recipe => <RecipeCard recipe={recipe} onClick={this.handleRecipeCardClick} key={recipe.recipeID} />)}
                            </Grid>
                    }
                </Grid>
            </Grid>
        );
    }
}

export default RecipeSearch;