import React from 'react';
import {Paper, Grid, Button, Typography, Hidden, Divider, Tabs, Tab, Box} from '@material-ui/core';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import RecordsPage from "../../Pages/Pages_Dashboard/RecordsPage";
import PhotosPage from "../../Pages/Pages_Dashboard/PhotosPage";
import {getRecipeDetails} from '../../Logic/Logic_Web/RecipesLogic';
import {fetchRecords} from '../../Logic/Logic_Dashboard/DevicesLogic';

/**
 * !Props:
 * * Variables
 *
 * * Methods
 * openSnackBar: HOC Methods to display a message via SnackBar
 * startLoading: HOC Methods to display LoadingOverlay
 * stopLoading : HOC Methods to hide LoadingOverlay
 * 
 * !States:
 * chosenRecipe: JSObject, represents details about the recipe (Name, Description, Owner, Age, etc)
 * recordsList : Array<JSObject>, used to show List of configuration and monitor data
 * photosList  : Array<JSObject>, used to show Cards detailing photos taken while the recipe is active
 */

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <Box
            component="div"
            role="tabpanel"
            hidden={value !== index}
            {...other}
        >
            {children}
        </Box>
    );
}

class RecipeDetails extends React.Component {
    targetID = null;

    constructor(props){
        super(props);
        this.handleTabChange = this.handleTabChange.bind(this);
        this.getRecords = this.getRecords.bind(this);
        this.getPhotos = this.getPhotos.bind(this);

        this.targetID = this.props.match.params['recipeID'];

        this.state = {
            chosenRecipe: null,
            recordsList: [],
            photoList: [],
        };

        getRecipeDetails(this.targetID).then(async recipeInfo => {
            if(recipeInfo === null){
                this.props.openSnackBar('No Such Recipe');
            } else {
                this.state.chosenRecipe = recipeInfo;
                await this.getRecords();
            }
        })
    }

    formatTime(startTime, endTime) {
        const timeDiff = (new Date(endTime)).valueOf() - (new Date(startTime)).valueOf();

        const dayDiff = Math.floor(timeDiff / (24 * 60 * 60 * 1000));
        const hourDiff = Math.floor((timeDiff % (24 * 60 * 60 * 1000)) / (60 * 60 * 1000));
        const minuteDiff = Math.floor((timeDiff % (60 * 60 * 1000)) / (60 * 1000));
        const secondDiff = Math.floor((timeDiff % (60 * 1000)) / (1000));

        const dayString = dayDiff === 0 ? '' : String(dayDiff) + ' day' + (dayDiff > 1 ? 's' : '');
        const hourString = hourDiff === 0 ? '' : (String(hourDiff).length === 1 ? '0' + String(hourDiff) : String(hourDiff)) + ' hours';
        const minuteString = minuteDiff === 0 ? '' : (String(minuteDiff).length === 1 ? '0' + String(minuteDiff) : String(minuteDiff)) + ' minutes';
        const secondString = secondDiff === 0 ? '' : (String(secondDiff).length === 1 ? '0' + String(secondDiff) : String(secondDiff)) + ' seconds';

        return (
            <React.Fragment>
                {dayString} <br />
                {hourString} <br />
                {minuteString} <br />
                {secondString}
            </React.Fragment>
        )
    }

    handleTabChange(e, chosenIndex) {
        this.setState({
            chosenTab: chosenIndex,
        });
    }

    async getRecords() {
        this.props.startLoading();
        if(this.state.chosenRecipe !== null){
            const recordsList = await fetchRecords(this.state.chosenRecipe.recipeID);
            this.setState({ recordsList: recordsList, });
        }
        this.props.stopLoading();
    }

    getPhotos() {
        const now = new Date();
        const past = new Date(2019, 8, 17, 14, 3, 22);

        const tempTimestamp = now.valueOf() - past.valueOf();
        const tempImageLink = "/img/logo192.png";

        this.setState({
            photoList: [
                {
                    photoAge: tempTimestamp,
                    imageLink: tempImageLink,
                },
                {
                    photoAge: tempTimestamp,
                    imageLink: tempImageLink,
                },
                {
                    photoAge: tempTimestamp,
                    imageLink: tempImageLink,
                },
                {
                    photoAge: tempTimestamp,
                    imageLink: tempImageLink,
                },
                {
                    photoAge: tempTimestamp,
                    imageLink: tempImageLink,
                },
                {
                    photoAge: tempTimestamp,
                    imageLink: tempImageLink,
                },
                {
                    photoAge: tempTimestamp,
                    imageLink: tempImageLink,
                },
            ],
        });

        //TODO: Re implement when the API is made
        // this.props.startLoading();
        // const photoList = await fetchPhotos(this.state.projectSelection.serialNumber);
        // this.setState({ photoList: photoList, });
        // this.props.stopLoading();
    }

    render() {
        return (
            <Paper elevation={5} style={{ padding: '32px' }}>
                <Grid container direction='row'>
                    <Grid item xs={12} md={3} container direction='row' alignItems='flex-start' alignContent='flex-start' justify='space-around' spacing={4}>
                        <Grid item xs={12}>
                            <Button size='large' onClick={() => {this.props.history.push('/recipes')}}>
                                <ArrowBackIosIcon /> Back
                            </Button>
                        </Grid>
                        <Grid item xs={12}>
                            {
                                this.state.chosenRecipe ?
                                    <img src={this.state.chosenRecipe.recipeImage} alt={this.state.chosenRecipe.recipeName + ' by ' + this.state.chosenRecipe.recipeOwner} style={{ objectFit: 'scale-down' }} />
                                    : null
                            }
                        </Grid>
                        <Grid item xs={12}>
                            {
                                this.state.chosenRecipe ?
                                <Typography variant='subtitle1'>
                                    {this.formatTime(this.state.chosenRecipe.recipeStart, this.state.chosenRecipe.recipeEnd)}
                                </Typography>
                                : null
                            }
                        </Grid>
                    </Grid>
                    <Hidden smDown>
                        <Grid md={1}>
                            <Divider orientation='vertical' variant='fullWidth' />
                        </Grid>
                    </Hidden>
                    <Grid item xs={12} md={8} container direction='row' alignContent='flex-start' justify='space-evenly' spacing={4}>
                        <Grid item xs={12}>
                            <Tabs
                                value={this.state.chosenTab}
                                onChange={this.handleTabChange}
                                indicatorColor='primary'
                                textColor='primary'
                                variant='fullWidth'
                            >
                                <Tab label='About' />
                                <Tab label='Photos' />
                                <Tab label='Records' />
                            </Tabs>
                        </Grid>
                        <Grid item xs={12} container direction='row' alignContent='flex-start' alignItems='flex-start'>
                            <TabPanel value={this.state.chosenTab} index={0}>
                                {
                                    this.state.chosenRecipe !== null ?
                                        <React.Fragment>
                                            <Grid item xs={12}>
                                                <Typography variant='h5'> {this.state.chosenRecipe.recipeName} </Typography>
                                            </Grid>

                                            <Grid item xs={12}>
                                                <Typography variant='subtitle1'> Recipe ID : {this.state.chosenRecipe.recipeID} </Typography>
                                            </Grid>

                                            <Grid item xs={12}>
                                                <Typography variant='overline'> By {this.state.chosenRecipe.recipeOwner} </Typography>
                                            </Grid>

                                            <Grid item xs={12}>
                                                <Typography variant='body1'> {this.state.chosenRecipe.recipeDesc} </Typography>
                                            </Grid>
                                        </React.Fragment>
                                        : null
                                }
                            </TabPanel>
                        </Grid>
                        <Grid item xs={12} container direction='row' justify='space-evenly' alignContent='flex-start' alignItems='flex-start'>
                            <TabPanel value={this.state.chosenTab} index={1}> 
                                <PhotosPage projectSelection={{recipeStart: this.state.chosenRecipe ? this.state.chosenRecipe.recipeStart : null}} fetchPhotos={this.getPhotos} photoList={this.state.photoList} />
                            </TabPanel>
                            <TabPanel value={this.state.chosenTab} index={2}>
                                <RecordsPage fetchRecords={this.getRecords} recordsList={this.state.recordsList} projectSelection={{recipeStart: this.state.chosenRecipe ? this.state.chosenRecipe.recipeStart : null}}/>
                            </TabPanel>
                        </Grid>
                    </Grid>
                </Grid>
            </Paper>
        );
    }
}

export default RecipeDetails;