import { Card, CardContent, CardHeader, Collapse, Grid } from "@material-ui/core";
import React from 'react';

/**
 * !Props
 * * Variables
 * avatar : <Avatar> Component, Left hand side Icon for decorative purpose
 * title  : String, The name of the card.
 */

class BigDisplay extends React.Component {    
    render() {
        return (
            <Grid item xs={12} md={6} lg={4}>
                <Card style={{ height: "250px"}}>
                    <CardHeader avatar={this.props.avatar} title={this.props.title} style={{paddingBottom:'0px'}}/>
                    <CardContent style={{paddingTop:"0px"}}>
                        {this.props.children}
                    </CardContent>
                    <Collapse in={true} timeout="auto" unmountOnExit>
                        <CardContent>
                        </CardContent>
                    </Collapse>
                </Card>
            </Grid>
        );
    }
}

export default BigDisplay;