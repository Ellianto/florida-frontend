import { FormControl, FormControlLabel, FormGroup, Grid, IconButton, Paper, Switch, Typography, Button, Hidden } from '@material-ui/core';
import { Save, SettingsApplications, Delete, Cancel } from '@material-ui/icons';
import React from 'react';

/**
 * !Props:
 * * Variables
 * editMode : Boolean, shows whether user is in Edit Mode or not
 * autoPilot: Boolean, shows whether device is in Auto Pilot Mode or not
 * recipe   : JSObject, information of the "Reference Recipe", if any
 * initTimestamp: Number, contains value of when the chosen active recipe starts. For timer purposes
 * 
 * * Methods
 * handleAddDevice  : Method to display "Add Device" Dialog
 * handleModeChange : Method to change HOC editMode state
 * handleAutoPilotChange        : Method to change the HOC autoPilot state
 * handleAddReferenceRecipe     : Method to add "reference recipe" when user clicks a button
 * handleRemoveReferenceRecipe  : Method to remove "reference recipe" when user clicks a button
 * 
 * !States:
 * dayDiff      : Number, represents day difference for the timer
 * hourDiff     : Number, represents hour difference for the timer
 * minuteDiff   : Number, represents minute difference for the timer
 * secondDiff   : Number, represents second difference for the timer
 * showRef      : Boolean, decides whether to show "reference recipe" Card, if any
 * showButton   : Boolean, decides whether to show certain buttons or not
 * showPlantAge : Boolean, decides whether to display current active recipe timer
 * showAddDevice: Boolean, decides whether to show "Add Device" button or no
 */

class StatusBar extends React.Component {
    constructor(props) {
        super(props);

        this.updateTimer = this.updateTimer.bind(this);

        this.timerVar = null;

        let showRef = false;
        let showButton = false;
        let showAddDevice = true;
        let showPlantAge = false;

        if (this.props.match.params.page === 'monitor') {
            showRef = true;
            showButton = true;
            showPlantAge = true;
            showAddDevice = false;
        } else if (this.props.match.params.page === 'photos' || this.props.match.params.page === 'records') {
            showPlantAge = true;
            showAddDevice = false;
        }

        this.state = {
            dayDiff: 0,
            hourDiff: 0,
            minuteDiff: 0,
            secondDiff: 0,
            showRef: showRef,
            showButton: showButton,
            showPlantAge: showPlantAge,
            showAddDevice: showAddDevice,
        }
    }

    componentDidMount(){
        this.timerVar = setInterval(() => { this.updateTimer() }, 1000);
    }

    componentWillUnmount(){
        clearInterval(this.timerVar);
    }

    componentDidUpdate(prevProps){
        if(prevProps.match !== this.props.match){
            let showRef = false;
            let showPlantAge = false;
            let showButton = false;
            let showAddDevice = true;

            if (this.props.match.params.page === 'monitor') {
                showRef = true;
                showButton = true;
                showPlantAge = true;
                showAddDevice = false;
            } else if (this.props.match.params.page === 'photos' || this.props.match.params.page === 'records') {
                showPlantAge = true;
                showAddDevice = false;
            }

            this.setState({
                showPlantAge: showPlantAge,
                showRef: showRef,
                showButton: showButton,
                showAddDevice : showAddDevice,
            });
        }
    }

    updateTimer() {
        const timeDiff = (new Date()).valueOf() - parseInt(this.props.initTimestamp);

        const dayDiff    = Math.floor(timeDiff / (24 * 60 * 60 * 1000));
        const hourDiff   = Math.floor((timeDiff % (24 * 60 * 60 * 1000)) / (60 * 60 * 1000));
        const minuteDiff = Math.floor((timeDiff % (60 * 60 * 1000)) / (60 * 1000));
        const secondDiff = Math.floor((timeDiff % (60 * 1000)) / (1000));

        this.setState({
            dayDiff: dayDiff,
            hourDiff: hourDiff,
            minuteDiff: minuteDiff,
            secondDiff: secondDiff,
        });
    }

    render() {
        return (
                <Grid container direction="row" alignItems='center' justify='flex-start' spacing={4} m={2} pl={2}>
                    {
                        !this.state.showAddDevice ? null :
                        <React.Fragment>
                            <Grid item xs={12} md={9}/>
                            <Grid item xs={12} md={3}>
                                <Button fullWidth edge='end' color="primary" variant='contained' onClick={this.props.handleAddDevice}>
                                    Add New Device
                                </Button>
                            </Grid>
                        </React.Fragment>
                    }
                    {
                        !this.state.showPlantAge ? null : 
                        <Grid item xs={9} sm>
                            <Typography variant='h5' style={{ color: 'white' }}> Plant's Age </Typography>
                            <Typography variant="h6" style={{color:'white'}}>
                                {this.state.dayDiff} days {this.state.hourDiff + ':' + this.state.minuteDiff + ':' + this.state.secondDiff}
                            </Typography>
                        </Grid>
                    }
                    {
                        !this.state.showButton ? null :
                        <Hidden smUp>
                            <Grid item xs={2}>
                                {
                                    !this.props.editMode ? null :
                                    <IconButton color="primary" onClick={() => {this.props.handleModeChange('cancel')}} style={{marginBottom:'0px'}}>
                                        <Cancel fontSize='large'/>
                                    </IconButton>
                                }
                                <IconButton color="secondary" onClick={this.props.editMode ? () => {this.props.handleModeChange('save')} : () => {this.props.handleModeChange('edit')}}>
                                    {this.props.editMode ? <Save fontSize="large" /> : <SettingsApplications fontSize="large" />}
                                </IconButton>
                            </Grid>
                        </Hidden>
                    }
                    {
                        this.state.showRef ? 
                        this.props.recipe == null ?
                            <Grid item xs={12} sm={6} md={7} lg={4}>
                                <Button style={{height: '72px'}} variant='contained' size="large" color="secondary" fullWidth onClick={this.props.handleAddReferenceRecipe}>
                                    <Typography variant='overline' style={{fontSize:'24px'}}>
                                        Add New Recipe    
                                    </Typography>    
                                </Button>
                            </Grid>
                            :
                            <Grid item xs={12} sm={6} md={7} lg={4}>
                                <Paper style={{height: '72px'}}>
                                    <Grid item container direction="row" justify="space-around" alignItems="center" xs={12}>
                                        <Grid item>
                                            <Typography variant="body1">
                                                {this.props.recipe.recipeName}
                                            </Typography>
                                            <Typography variant="subtitle2">
                                                Recipe ID : {this.props.recipe.recipeID}
                                            </Typography>
                                            <Typography variant="subtitle2">
                                                By : {this.props.recipe.recipeOwner}
                                            </Typography>
                                        </Grid>
                                        <Grid item>
                                            <FormControl>
                                                <FormGroup row>
                                                    <FormControlLabel
                                                        label="Auto"
                                                        labelPlacement="top"
                                                        control={
                                                            <Switch 
                                                                disabled={!this.props.editMode}
                                                                checked={this.props.autoPilot}
                                                                onChange={this.props.handleAutoPilotChange}
                                                                color="secondary" pt={2}
                                                            />
                                                        }
                                                    />
                                                </FormGroup>
                                            </FormControl>
                                        </Grid>
                                        <Grid item>
                                            <IconButton color="inherit" edge="end" size="medium" onClick={this.props.handleRemoveReferenceRecipe}>
                                                <Delete p={2} />
                                            </IconButton>
                                        </Grid>
                                    </Grid>
                                </Paper>
                            </Grid>
                        : null
                    }
                    {
                        !this.state.showButton ? null :
                        <Hidden xsDown>
                            <Grid item xs={2} sm={1}>
                                {
                                    !this.props.editMode ? null :
                                        <IconButton color="primary" onClick={() => { this.props.handleModeChange('cancel') }} style={{ marginBottom: '0px' }}>
                                            <Cancel fontSize='large' />
                                        </IconButton>
                                }
                                <IconButton color="secondary" onClick={this.props.editMode ? () => {this.props.handleModeChange('save')} : () => {this.props.handleModeChange('edit')}}>                                
                                    {this.props.editMode ? <Save fontSize="large" /> : <SettingsApplications fontSize="large" />}
                                </IconButton>
                            </Grid>
                        </Hidden>
                    }
                </Grid>
        );
    }
}

export default StatusBar;