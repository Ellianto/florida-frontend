import React from 'react';
import {Dialog, DialogContent, DialogContentText, DialogTitle, DialogActions, Button, TextField} from '@material-ui/core';
import {createDevice, createRecipe, endRecipe, updateConfig} from '../../Logic/Logic_Dashboard/DevicesLogic';
import {getRecipeDetails} from '../../Logic/Logic_Web/RecipesLogic';

/**
 * !Props:
 * * Variables
 * configID : current configuration ID, to be passed when referring a recipe
 * autoPilot : autoPilot Switch condition, to be passed when referring a recipe
 * projectSelection : JSObject containing details of current device and recipe
 * openHarvestDialog : Boolean, decides whether to show the "Harvest Dialog" or not
 * openNewRecipeDialog : Boolean, decides whether to show the "New Recipe Dialog" or not
 * openNewDeviceDialog : Boolean, decides whether to show the "New Device Dialog" or not
 * openReferRecipeDialog : Boolean, decides whether to show the "Refer Recipe Dialog" or not
 *
 * * Methods
 * closeDialog : Method to close the dialog
 * startLoading: Method to open the LoadingOverlay
 * stopLoading : Method to close the LoadingOverlay
 * openSnackBar: Method to show the SnackBar with a specific message
 * fetchDeviceList : Method to send API Request and fetch Device List
 * handleProjectChange : Method to change the projectSelection
 * getCurrentConfiguration : Method to get the current configuration from another component
 * handleAddReferenceRecipe : Method to send API Request and add reference recipe
 *
 * !States:
 * newDeviceName : String, the device name that is about to be created
 * newRecipeName : String, the recipe name that is about to be created
 * newRecipeDesc : String, the recipe description that is about to be created
 * confirmRecipeID : String, works as a confirmation when user wants to "Harvest"
 * referenceRecipeID : String, the recipe ID that the user wants to refer
 * confirmRecipeIDError : Boolean, shows whether the user types the correct recipe ID when about to "Harvest"
 */

class Dialogs extends React.Component{
    constructor(props){
        super(props);

        this.handleHarvestRecipe = this.handleHarvestRecipe.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleNewRecipe = this.handleNewRecipe.bind(this);
        this.handleNewDevice = this.handleNewDevice.bind(this);
        this.referToRecipe = this.referToRecipe.bind(this);

        this.state = {
            newDeviceName: '',
            newRecipeName: '',
            newRecipeDesc: '',
            confirmRecipeID: '',
            referenceRecipeID: '',
            confirmRecipeIDError: false,
        }
    }

    async handleHarvestRecipe(){
        this.props.closeDialog('openHarvestDialog');

        this.props.startLoading();
        
        if(parseInt(this.state.confirmRecipeID,10) === this.props.projectSelection.recipeID){
            const recipeHarvested = await endRecipe(this.props.projectSelection.recipeID);
            if(recipeHarvested){
                this.props.openSnackBar('Recipe Harvested Successfully');
                window.location.reload();
            }
        } else {
            this.props.openSnackBar('Please input the correct Recipe ID');
            this.setState({
                confirmRecipeIDError: true,
                confirmRecipeID: '',
            });
        }
        
        this.props.stopLoading();
    }

    async handleNewRecipe(){
        this.props.closeDialog('openNewRecipeDialog');
        
        this.setState({newRecipeName: '', newRecipeDesc: '',});
        
        this.props.startLoading();
        
        const newRecipeID = await createRecipe(this.state.newRecipeName, this.state.newRecipeDesc, this.props.projectSelection.serialNumber);
        
        this.props.stopLoading();
        
        await this.props.fetchDeviceList();
    }

    async handleNewDevice(){
        this.props.closeDialog('openNewDeviceDialog');
        
        this.setState({newDeviceName: '',});
        
        this.props.startLoading();
        
        const newSerialNum = await createDevice(this.state.newDeviceName);
        
        this.props.stopLoading();
        
        await this.props.fetchDeviceList();
    }

    async referToRecipe(){
        this.props.closeDialog('openReferRecipeDialog');

        this.setState({ referenceRecipeID: '',});
        
        this.props.startLoading();

        let message = 'Something wrong occured! Please refresh the browser to try again!';
        const currConf = this.props.getCurrentConfiguration();

        const newConfig = {
            id: this.props.configID,
            tds_value: currConf.tds,
            nutrisiA: currConf.nutrientA,
            nutrisiB: currConf.nutrientB,
            lamp_uv_enable: currConf.colorSwitch,
            lamp_grow_enable: currConf.whiteSwitch,
            lamp_uv_timer: currConf.colorTimeList,
            lamp_grow_timer: currConf.whiteTimeList,
            auto_pilot: this.props.autoPilot,
            main_recipe: this.props.projectSelection.recipeID,
            active_recipe: this.state.referenceRecipeID,
        }

        const updateOK = await updateConfig(newConfig, this.props.projectSelection.serialNumber);

        if(updateOK){
            const referenceRecipe = await getRecipeDetails(this.state.referenceRecipeID);

            if(referenceRecipe === null){
                message = 'Failed to find reference recipe!';
            } else {
                //TODO: Replace Dumb data
                const newReferenceRecipe = {
                    recipeName: referenceRecipe.recipeName,
                    recipeID: referenceRecipe.recipeID,
                    recipeStart: referenceRecipe.recipeStart,
                    recipeOwner: referenceRecipe.recipeOwner,
                    data: {
                        tds: 982,
                        illuminance: 812,
                        nutrients: '3:9',
                        humidity: 39,
                        temperature: 28,
                    }
                };

                this.props.handleAddReferenceRecipe(newReferenceRecipe)
                message = 'Successfully referred recipe';
            }
        }

        this.props.openSnackBar(message);
        
        this.props.stopLoading();
    }

    handleInputChange(e){
        this.setState({
            confirmRecipeIDError : false,
            [e.target.name] : e.target.value,
        });
    }

    render(){
        return(
            <React.Fragment>
                <Dialog id='harvest-dialog' open={this.props.openHarvestDialog} onClose={() => {this.setState({confirmRecipeID: ''});this.props.closeDialog('openHarvestDialog')}}>
                    <DialogTitle> Finish Recipe? </DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            This will complete the current recipe for the device. This action cannot be undone! Type this recipe's ID ({this.props.projectSelection === null ? '' : this.props.projectSelection.recipeID}) to continue:
                        </DialogContentText>
                        <TextField required fullWidth autoFocus
                            type='number'
                            margin='dense'
                            name='confirmRecipeID'
                            autoComplete={false}
                            onChange={this.handleInputChange}
                            value={this.state.confirmRecipeID}
                            error={this.state.confirmRecipeIDError}
                            inputProps={{
                                max: 1000000000,
                                min: 1,
                            }}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button color="secondary" onClick={() => {this.setState({confirmRecipeID:'',}); this.props.closeDialog('openHarvestDialog');}}>
                            Cancel
                        </Button>
                        <Button color="primary" onClick={this.handleHarvestRecipe}>
                            Harvest
                        </Button>
                    </DialogActions>
                </Dialog>
                <Dialog id='new-recipe' open={this.props.openNewRecipeDialog} onClose={() => {this.setState({newRecipeName: ''}); this.props.closeDialog('closeNewRecipeDialog')}}>
                    <DialogTitle> Create New Recipe </DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Create a new recipe for the device {this.props.projectSelection === null ? '' : this.props.projectSelection.deviceName}
                        </DialogContentText>
                        <TextField required fullWidth autoFocus
                            type='text'
                            label='Recipe Name'
                            name='newRecipeName'
                            autoComplete={false}
                            value={this.state.newRecipeName}
                            onChange={this.handleInputChange}
                            style={{marginBottom: '16px',}}
                            inputProps={{
                                maxlength: 100,
                                minlength: 8,
                            }}
                        />
                        <TextField required fullWidth
                            type='text'
                            name='newRecipeDesc'
                            label='Recipe Description'
                            autoComplete={false}
                            value={this.state.newRecipeDesc}
                            onChange={this.handleInputChange}
                            inputProps={{
                                maxlength: 200,
                                minlength: 20,
                            }}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button color="secondary" onClick={() => { this.setState({ newRecipeName: '' }); this.props.closeDialog('closeNewRecipeDialog') }}>
                            Cancel
                        </Button>
                        <Button color="primary" onClick={this.handleNewRecipe}>
                            Submit
                        </Button>
                    </DialogActions>
                </Dialog>
                <Dialog id='refer-recipe' open={this.props.openReferRecipeDialog} onClose={() => {this.setState({referenceRecipeID: ''}); this.props.closeDialog('openReferRecipeDialog')}}>
                    <DialogTitle> Choose Reference Recipe </DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Choose a recipe to refer to. The reference recipe's configuration can be used to compare progress or to automate the Food Computer.
                        </DialogContentText>
                        <TextField required fullWidth autoFocus
                            type='number'
                            margin='dense'
                            name='referenceRecipeID'
                            autoComplete={false}
                            value={this.state.referenceRecipeID}
                            onChange={this.handleInputChange}
                            inputProps={{
                                max: 1000000000,
                                min: 1,
                            }}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button color='secondary' onClick={() => { this.setState({ referenceRecipeID: '' }); this.props.closeDialog('openReferRecipeDialog')}}>
                            Cancel
                        </Button>
                        <Button color='primary' onClick={this.referToRecipe}>
                            Create Reference
                        </Button>
                    </DialogActions>
                </Dialog>
                <Dialog id='new-device' open={this.props.openNewDeviceDialog} onClose={() => {this.setState({ newDeviceName: '' }); this.props.closeDialog('openNewDeviceDialog') }}>
                    <DialogTitle> Create New Device </DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Create a new Device to configure a new system. You need to create a new device in order to get a new Device ID. This newly created ID will be used in the initial setup of the new system.
                        </DialogContentText>
                        <TextField required fullWidth autoFocus
                            type='text'
                            margin='dense'
                            name='newDeviceName'
                            value={this.state.newDeviceName}
                            onChange={this.handleInputChange}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button color='secondary' onClick={() => { this.setState({ newDeviceName: '' }); this.props.closeDialog('openNewDeviceDialog') }}>
                            Cancel
                        </Button>
                        <Button color='primary' onClick={this.handleNewDevice}>
                            Create Devices
                        </Button>
                    </DialogActions>
                </Dialog>
            </React.Fragment>
        )
    }
}

export default Dialogs;