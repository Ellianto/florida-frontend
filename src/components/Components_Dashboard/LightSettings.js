import DateFnsUtils from '@date-io/date-fns';
import { FormControl, FormControlLabel, Grid, IconButton, Switch, Table, TableBody, TableCell, TableHead, TableRow, Typography } from '@material-ui/core';
import { AddRounded, RemoveRounded } from '@material-ui/icons';
import { KeyboardTimePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import React from 'react';
import {lightFormat} from 'date-fns';

/** 
 *  !Props:
 *  * Variables
 *  onColor = String, The color of the ring to display when Switch is ON
 *  color   = String, The color of the Switch to display when Switch is ON
 *  isChecked = Boolean, Configuration information of whether the Switch is ON/OFF.
 *  autoPilot = Boolean, Configuration information of whether the device is in AutoPilot mode or not
 *  editMode  = Boolean, UI information of whether the user is about to Edit or not. For disabling the Inputs
 *  timeList  = Array<JSObject>, fetched from API to display/modify here
 * 
 *  * Methods
 *  timeListAdd = Method to add entry to the specified timeList Array
 *  timeListRemove = Method to remove entry to the specified timeList Array
 *  
 *  !States:
 *  start: local state used when user wants to add new entry to the timeListArray. Specifies "start time"
 *  end: local state used when user wants to add new entry to the timeListArray. Specifies "end time"
 */
class LightSettings extends React.Component {
    
    constructor(props) {
        super(props);

        this.timeFormat = this.timeFormat.bind(this);
        this.handleEndTimeChange = this.handleEndTimeChange.bind(this);
        this.handleStartTimeChange = this.handleStartTimeChange.bind(this);
        
        this.state = {
            start: new Date(),
            end: new Date(),
        }
    }

    handleStartTimeChange(time){
        this.setState({
            start : time,
        });
    }

    handleEndTimeChange(time){
        this.setState({
            end: time,
        })
    }

    timeFormat(timestamp){
        const timeArr = timestamp.split(':');
        const now = new Date();
        const timeVal = new Date(now.getFullYear(), now.getMonth(), now.getDate(), parseInt(timeArr[0], 10), parseInt(timeArr[1], 10), parseInt(timeArr[2],10));
        return lightFormat(timeVal, "HH:mm");
    }
    
    render() {
        return (
            <Grid container>
                <Grid item xs={4}>
                    <FormControl>
                        <FormControlLabel
                            label="Lights"
                            labelPlacement="top"
                            control={
                                <Switch checked={this.props.isChecked}
                                    color={this.props.color}
                                    onChange={this.props.handleSwitchChange}
                                    disabled={!(!this.props.autoPilot && this.props.editMode)}
                                />
                            }
                            style={{
                                marginTop:'50%',
                                width: "72px",
                                height: "72px",
                                borderRadius: "50%",
                                borderColor: this.props.isChecked ? this.props.onColor : "black",
                                borderStyle: "inset",
                            }}
                        />
                    </FormControl>
                </Grid>
                <Grid item xs={8}>
                    <Table size="small" padding="none">
                        <TableHead>
                            <TableRow>
                                <TableCell align='center'> Start </TableCell>
                                <TableCell align='center'> End </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                this.props.timeList.length > 0 ?
                                    this.props.timeList.map((time, idx) => {
                                        if (time.start === time.end) return null;
                                        return (
                                            <TableRow key={time.id}>
                                                <TableCell> <Typography variant="body1" align='center'> {this.timeFormat(time.start)} </Typography> </TableCell>
                                                <TableCell> <Typography variant="body1" align='center'> {this.timeFormat(time.end)} </Typography> </TableCell>
                                                <TableCell>
                                                    {
                                                        !this.props.autoPilot && this.props.editMode ?
                                                            <IconButton size='small' onClick={() => { this.props.timeListRemove(idx) }}>
                                                                <RemoveRounded />
                                                            </IconButton>
                                                            : null
                                                    }
                                                </TableCell>
                                            </TableRow>
                                        )
                                    })
                                :
                                    null
                            }
                            {
                                !this.props.editMode ? null :(
                                this.props.timeList.some(val => val.start === val.end) ?
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <TableRow>
                                        <TableCell>
                                            <FormControl>
                                                <KeyboardTimePicker
                                                    disabled={!(!this.props.autoPilot && this.props.editMode)}
                                                    ampm={false}
                                                    variant='outlined'
                                                    margin="dense"
                                                    id="start"
                                                    name="start"
                                                    value={this.state.start}
                                                    onChange={this.handleStartTimeChange}
                                                    KeyboardButtonProps={{
                                                        'aria-label': 'change start time',
                                                    }}
                                                />
                                            </FormControl>
                                        </TableCell>
                                        <TableCell>
                                            <FormControl>
                                                <KeyboardTimePicker
                                                    disabled={!(!this.props.autoPilot && this.props.editMode)}
                                                    ampm={false}
                                                    variant='outlined'
                                                    margin="dense"
                                                    id="end"
                                                    name="end"
                                                    value={this.state.end}
                                                    onChange={this.handleEndTimeChange}
                                                    KeyboardButtonProps={{
                                                        'aria-label': 'change end time',
                                                    }}
                                                />
                                            </FormControl>
                                        </TableCell>
                                        <TableCell>
                                            {
                                                (!this.props.autoPilot && this.props.editMode) ?
                                                <IconButton size='small' onClick={() => {this.props.timeListAdd({start: this.state.start, end: this.state.end,})}}>
                                                    <AddRounded />
                                                </IconButton>
                                                :
                                                null
                                            }
                                        </TableCell>
                                    </TableRow>
                                </MuiPickersUtilsProvider>
                                : null
                                )
                            }
                        </TableBody>
                    </Table>
                </Grid>
            </Grid>
        );
    }
}

export default LightSettings;