import { Divider, FormControl, FormControlLabel, FormGroup, Grid, IconButton, Paper, Switch, Typography } from '@material-ui/core';
import Delete from '@material-ui/icons/Delete';
import React from 'react';

/**
 * !Props:
 * * Variables
 * referenceName : String, Name of the referenced Recipe
 * referenceID   : String, ID of the reference Recipe
 * referenceUser : String. Referenced Recipe Owner's Username
 * autoPilot     : Boolean. The status of the Auto Pilot Mode
 * 
 * * Methods
 * handleAutoPilotChange : Handling AUTOPILOT Switching
 * handleDeleteReference : Handling Delete Button Click
 */

class RecipeCard extends React.Component {
    render() {
        return (
            <Paper style={{ margin: "8px", padding: "8px", width: "450px" }}>
                <Grid item>
                    <Typography variant="h6">
                        {this.props.referenceName}
                    </Typography>
                    <Typography variant="subtitle1">
                        Recipe ID : {this.props.referenceID}
                    </Typography>
                    <Typography variant="subtitle1">
                        By : {this.props.referenceUser}
                    </Typography>
                </Grid>
                <Grid item>
                    <Divider orientation="vertical" />
                </Grid>
                <Grid item>
                    <FormControl>
                        <FormGroup row>
                            <FormControlLabel
                                label="Autopilot"
                                labelPlacement="top"
                                control={
                                    <Switch checked={this.props.autoPilot}
                                        onChange={this.props.handleAutoPilotChange}
                                        color="secondary" pt={2}
                                    />
                                }
                            />
                        </FormGroup>
                    </FormControl>
                </Grid>
                <Grid item>
                    <IconButton color="inherit" edge="end" size="medium">
                        <Delete p={2} />
                    </IconButton>
                </Grid>
            </Paper>
        );
    }
}

export default RecipeCard;