import { Card, CardContent, CardHeader, Collapse, Grid, Typography } from '@material-ui/core';
import React from 'react';

/**
 * !Props:
 * * Variables
 * avatar : <Avatar> Component, Left hand side Icon for decorative purpose
 * title  : String, The name of the card.
 * referenceValue : Number/String, Value from "Reference Recipe", if any
 */

class SmallDisplay extends React.Component {
    render() {
        return (
            <Grid item xs={12} sm={6} md={3} lg={2}>
                <Card style={{ height: "250px" }}>
                    <CardHeader avatar={this.props.avatar} title={this.props.title} />
                    <CardContent>
                        <Grid container direction='row'>
                        {this.props.children}
                        {
                            this.props.referenceValue === null ?
                            null : 
                            <Grid item xs={6} style={{marginTop:'16px'}}>   
                                <Typography variant="subtitle2"> Ref. Value </Typography>
                                <Typography variant="h5"> {this.props.referenceValue} </Typography>
                                <Typography variant="overline"> {this.props.unitOfMeasurement} </Typography>
                            </Grid>   
                        }
                        </Grid>
                    </CardContent>
                    {
                        // this.props.referenceValue === null ?
                        // null : 
                        // <Collapse in={true} timeout="auto" unmountOnExit>
                        //     <CardContent>
                                
                        //     </CardContent>
                        // </Collapse>
                    }
                </Card>
            </Grid>
        );
    }
}

export default SmallDisplay;