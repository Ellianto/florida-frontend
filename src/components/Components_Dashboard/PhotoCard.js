import React, { Component } from 'react';
import {Grid, Card, CardContent, CardMedia, Box, Typography, Button} from '@material-ui/core';
import {PhotoCamera} from '@material-ui/icons';

/**
 * !Props:
 * * Variables
 * disabled : Boolean, toggled when user enters/exits Edit Mode. Disables/Enables button
 * imageLink : String, the URL of the latest photo taken for the chosen active recipe
 * 
 * * Methods
 * sendCameraCommand: Method passed from HOC that sends MQTT Message to take a photo
 * 
 */
class PhotoCard extends Component {
    render() {
        return (
            <Grid item xs={12} md={9} lg={6}>
                <Card style={{ height: '250px' }}>
                    <Grid container direction='row' justifyContent='center'>
                        <Grid item xs={4}>
                            <CardContent>
                                <Typography variant='body2'> <PhotoCamera /> Camera </Typography>
                            </CardContent>
                            <Box style={{ padding: '16px' }}>
                                <Button edge='start' disabled={this.props.disabled} style={{ marginTop: '12px' }} variant='contained' color="primary" fullWidth onClick={this.props.sendCameraCommand}> Take a Picture </Button>
                            </Box>
                        </Grid>
                        <Grid item xs={8}>
                            <CardMedia component="img" src={this.props.imageLink} alt="Plant Photo" title="Placeholder" style={{ objectFit: 'fit', width:'420px'}} />
                        </Grid>
                    </Grid>
                </Card>
            </Grid>
        );
    }
}

export default PhotoCard;