import React from 'react';
import {Snackbar, Typography} from '@material-ui/core';
import {Switch, Route, BrowserRouter as Router} from 'react-router-dom';
import LoadingOverlay from 'react-loading-overlay';

import Home from './Home';
import Dashboard from './Dashboard';
import LoginPage from './Pages/Pages_Web/LoginPage';
import RegisterPage from './Pages/Pages_Web/RegisterPage';
import { checkToken, getUserData } from './Logic/Logic_Web/AuthLogic';
class App extends React.Component {
    constructor(props) {
        super(props);

        this.logOut = this.logOut.bind(this);
        this.setUser = this.setUser.bind(this);
        this.clearUser = this.clearUser.bind(this);
        this.handleStopLoading = this.handleStopLoading.bind(this);
        this.handleStartLoading = this.handleStartLoading.bind(this);
        this.handleOpenSnackBar = this.handleOpenSnackBar.bind(this);
        this.handleCloseSnackBar = this.handleCloseSnackBar.bind(this);
        
        this.state = {
            user: {
                name: '',
                token: null,
            },
            snackBar: {
                open: false,
                message: '',
            },
            isLoading: false,
        }
    }

    clearUser(){
        this.setState({
            user: {
                name: '',
                token: null,
            }
        });
    }

    async componentDidMount(){
        this.handleStartLoading();
        if(localStorage.getItem('AuthToken') !== null){
            //There is a token stored in the Local Storage, try to authenticate
            const success = await checkToken();
            if(success){
                await this.setUser();
            } else {
                await this.logOut();
            }
        } else {
            this.setState({
                user:{
                    name: '',
                    token: null,
                }
            })
        }
        this.handleStopLoading();
    }

    async setUser(){
        const userToken = localStorage.getItem('AuthToken');
        const userData = await getUserData();
        let userName = '';

        if(userData === null){
            userName = 'Florida EKA';
        } else {
            userName = userData.first_name + ' ' + userData.last_name;
        }

        await this.setState({
            user : {
                name: userName,
                token: userToken,
            },
        });

        this.handleOpenSnackBar('Welcome ' + userName + '!');
    }

    async logOut(){
        await this.clearUser();
        localStorage.removeItem('AuthToken');
        window.location.href = window.location.origin;
    }

    handleOpenSnackBar(message){
        this.setState({
            snackBar: {
                open: true,
                message: message,
            }
        });
    }

    handleCloseSnackBar(){
        this.setState({
            snackBar:{
                open: false,
                message: '',
            }
        });
    }

    handleStartLoading(){
        this.setState({
            isLoading: true,
        });
    }

    handleStopLoading(){
        this.setState({
            isLoading: false,
        });
    }

    render() {
        const authFormStyle = { marginTop: "128px", marginLeft: "256px", width: "85%" };

        return (
            <Router>
                <Snackbar
                    open={this.state.snackBar.open}
                    anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                    onClose={this.handleCloseSnackBar}
                    message={<Typography variant='caption'> {this.state.snackBar.message} </Typography>}
                    autoHideDuration={3500}
                />
                <LoadingOverlay
                    active={this.state.isLoading}
                    text='Wait for a moment...'
                    spinner
                >
                    <Switch>
                        <Route exact path='/login' render={props => <LoginPage {...props} startLoading={this.handleStartLoading} stopLoading={this.handleStopLoading} openSnackBar={this.handleOpenSnackBar} style={authFormStyle} clearUser={this.clearUser} setUser={this.setUser} />} />
                        <Route exact path='/signup' render={props => <RegisterPage {...props} startLoading={this.handleStartLoading} stopLoading={this.handleStopLoading} openSnackBar={this.handleOpenSnackBar} style={authFormStyle} clearUser={this.clearUser}/>}/>
                        <Route path='/dashboard' render={props => <Dashboard {...props} startLoading={this.handleStartLoading} stopLoading={this.handleStopLoading} openSnackBar={this.handleOpenSnackBar} user={this.state.user} logOut={this.logOut}/>}/>
                        <Route path='/' render={props => <Home {...props} openSnackBar={this.handleOpenSnackBar} userToken={this.state.user.token} setUser={this.setUser} logOut={this.logOut} startLoading={this.handleStartLoading} stopLoading={this.handleStopLoading}/>} />
                    </Switch>
                </LoadingOverlay>
            </Router>
        );
    }
}

export default App;