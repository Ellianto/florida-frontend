import { AppBar, Avatar, Box, Button, ClickAwayListener, Drawer, Hidden, Paper, IconButton, List, ListItem, ListItemText, MenuList, MenuItem, Popover, TextField, Toolbar, Typography } from '@material-ui/core';
import { Assignment, BurstMode, Eco, Home, Memory, Menu, PersonalVideo } from '@material-ui/icons';
import { NavLink, Redirect, Route, Switch } from 'react-router-dom';
import React from 'react';
import Dialogs from './components/Components_Dashboard/Dialogs';
import StatusBar from './components/Components_Dashboard/StatusBar';
import PhotosPage from './Pages/Pages_Dashboard/PhotosPage';
import DevicesPage from './Pages/Pages_Dashboard/DevicesPage';
import MonitorPage from './Pages/Pages_Dashboard/MonitorPage';
import RecordsPage from './Pages/Pages_Dashboard/RecordsPage';

import {fetchDeviceList, fetchRecords, fetchPhotos, updateConfig} from './Logic/Logic_Dashboard/DevicesLogic';

/**
 * !Props:
 * * Variables
 * user        : JSObject, contains information about the currently logged in user
 * 
 * * Methods
 * openSnackBar: HOC Methods to display a message via SnackBar
 * startLoading: HOC Methods to display LoadingOverlay
 * stopLoading : HOC Methods to hide LoadingOverlay
 * logOut      : HOC Methods to logically "log out"
 * 
 * !States:
 * photoList            : Array<JSObject>, contains list of photo information of the currently active recipe
 * deviceList           : Array<JSObject>, contains list of "logical device" info of the currently logged in user
 * recordsList          : Array<JSObject>, contains list of records (configuration and monitor data) of the currently active recipe
 * configID             : String/Number, represents the ID of the currently active and running configuration 
 * editMode             : Boolean, represent the UI Mode the user is currently interacting with
 * autoPilot            : Boolean, represent whether the device is in Auto Pilot Mode or not
 * userMenuRef          : ?, Passed to the Popover component and parsed as Boolean for UI Purposes
 * projectSelection     : JSObject, contains information of the currently selected device and recipe
 * referenceRecipe      : JSObject, contains information of the currently referenced "reference recipe", if any
 * openMobileDrawer     : Boolean, used to show/hide the Drawer in Mobile View
 * openHarvestDialog    : Boolean, used to show/hide the Harvest Dialog
 * openNewRecipeDialog  : Boolean, used to show/hide the New Recipe Dialog
 * openNewDeviceDialog  : Boolean, used to show/hide the New Device Dialog
 * openReferRecipeDialog: Boolean, used to show/hide the Refer To Recipe Dialog
 * drawerWidth          : Number, used to manipulate UI Parameters when the viewport changes
 */


//TODO: Make 404 Page
class Dashboard extends React.Component {
    oldSettings = null;
    oldAutoPilot = null;

    constructor(props) {
        super(props);

        this.handleHarvest = this.handleHarvest.bind(this);
        this.handleNewRecipe = this.handleNewRecipe.bind(this);
        this.handleAddDevice = this.handleAddDevice.bind(this);
        this.updateDimensions = this.updateDimensions.bind(this);
        this.handleModeChange = this.handleModeChange.bind(this);
        this.handleOpenDialog = this.handleOpenDialog.bind(this);
        this.handleAvatarClick = this.handleAvatarClick.bind(this);
        this.handleCloseDialog = this.handleCloseDialog.bind(this);
        this.handleCloseUserMenu = this.handleCloseUserMenu.bind(this);
        this.handleProjectChange = this.handleProjectChange.bind(this);
        this.handleAutoPilotChange = this.handleAutoPilotChange.bind(this);
        this.handleOpenMobileDrawer = this.handleOpenMobileDrawer.bind(this);
        this.handleAddReferenceRecipe = this.handleAddReferenceRecipe.bind(this);
        this.handleRemoveReferenceRecipe = this.handleRemoveReferenceRecipe.bind(this); 
        this.wrapperGetConfig = this.wrapperGetConfig.bind(this);
        this.fetchDeviceList = this.fetchDeviceList.bind(this);
        this.checkAutoPilot = this.checkAutoPilot.bind(this);
        this.setConfigID = this.setConfigID.bind(this);
        this.getRecords = this.getRecords.bind(this);
        this.getPhotos = this.getPhotos.bind(this);

        this.drawerLinks = [
            {
                key: "logo",
                link: '/',
                component: <img src="/img/Logo_ColourBlack.png" alt="" width={192} />,
                icon: <IconButton color="primary" size="small"> <Home /> </IconButton>,
            },
            {
                key: "devices",
                link: '/dashboard/devices',
                component: <Typography variant='overline' style={{ fontSize: 'large' }}>Devices</Typography>,
                icon: <IconButton color="primary" size="small"> <Memory /> </IconButton>
            },
            {
                key: "monitor",
                link: '/dashboard/monitor',
                component: <Typography variant='overline' style={{ fontSize: 'large' }}>Monitoring</Typography>,
                icon: <IconButton color="primary" size="small"> <PersonalVideo /> </IconButton>
            },
            {
                key: "records",
                link: '/dashboard/records',
                component: <Typography variant='overline' style={{ fontSize: 'large' }}>Records</Typography>,
                icon: <IconButton color="primary" size="small"> <Assignment /> </IconButton>
            },
            {
                key: "photos",
                link: '/dashboard/photos',
                component: <Typography variant='overline' style={{ fontSize: 'large' }}>Photos</Typography>,
                icon: <IconButton color="primary" size="small"> <BurstMode /> </IconButton>
            },
            {
                key: "harvest",
                link: null,
                component: <Button variant="contained" size="large" fullWidth={true} color="primary" onClick={this.handleHarvest}> Harvest </Button>,
                icon: <IconButton variant="contained" color="primary" size="small" onClick={this.handleHarvest}> <Eco /> </IconButton>
            },
        ];

        this.state = {
            photoList:[],
            deviceList:[],
            recordsList:[],
            configID: null,
            editMode: false,
            autoPilot: false,
            userMenuRef: null,
            projectSelection:null,
            referenceRecipe: null,
            openMobileDrawer: false,
            openHarvestDialog: false,
            openNewRecipeDialog: false,
            openNewDeviceDialog: false,
            openReferRecipeDialog: false,
            drawerWidth: window.innerWidth > 960 ? 225 : 0,
        }
    }

    componentDidMount() {
        // * Adds listener to when window is resized
        window.addEventListener('resize', this.updateDimensions);
        this.fetchDeviceList();
    }

    componentWillUnmount() {
        // * removes the listener when component unmounts
        window.removeEventListener('resize', this.updateDimensions);
    }

    /* Front End Logic Methods */

    async fetchDeviceList() {
        this.props.startLoading();

        const deviceList = await fetchDeviceList();

        if (deviceList === null) {
            this.props.openSnackBar("No Devices Found!");
        } else {
            this.setState({ deviceList: deviceList, });
        }

        this.props.stopLoading();
    }

    async getRecords(){
        this.props.startLoading();
        const recordsList = await fetchRecords(this.state.projectSelection.recipeID);
        this.setState({ recordsList: recordsList, });
        this.props.stopLoading();
    }

    async getPhotos(){
        this.props.startLoading();
        const photoList = await fetchPhotos(this.state.projectSelection.serialNumber);
        this.setState({ photoList : photoList, });
        this.props.stopLoading();
    }

    async handleModeChange(mode) {
        this.props.startLoading();

        if (mode === 'save') {
            const success = await this.refs.monitorPage.postConfiguration();

            if (!success){
                await this.refs.monitorPage.handleConfigChange(this.oldSettings);
                this.oldSettings = null;
            }
        } else if (mode === 'cancel') {
            await this.refs.monitorPage.handleConfigChange(this.oldSettings);
            this.oldSettings = null;
            this.setState({
                autoPilot: this.oldAutoPilot,
            })
        } else if (mode === 'edit') {
            this.oldSettings = await this.refs.monitorPage.getConfigState();
            this.oldAutoPilot = this.state.autoPilot;
        }

        //Inverts the previous state
        this.setState((state, props) => ({
            editMode: !state.editMode,
        }));

        this.props.stopLoading();
    }

    async handleProjectChange(project) {
        const pathName = this.props.history.location.pathname.split('/')[2];

        if(pathName === 'monitor'){
            this.refs.monitorPage.handleMQTTChangeTopic();
        }

        await this.setState({
            projectSelection: {
                deviceID: project.deviceID,
                deviceName: project.deviceName,
                serialNumber: project.serialNumber,
                recipeID: project.recipeID,
                recipeName: project.recipeName,
                recipeStart: project.recipeStart,
            }
        });

        this.props.openSnackBar(project.deviceName + ' selected!');

        if(pathName === 'monitor'){
            this.refs.monitorPage.fetchConfig();
            this.refs.monitorPage.handleMQTTConnectionEstablished();
        } else if(pathName === 'photos'){
            this.fetchPhotos();
        } else if(pathName === 'records'){
            this.fetchRecords();
        }
    }

    async handleRemoveReferenceRecipe() {
        this.props.startLoading();

        let message = 'Something wrong occured! Please refresh the browser to try again!';

        const currConf = await this.refs.monitorPage.getConfigState();

        const newConfig = {
            id: this.state.configID,
            tds_value: currConf.tds,
            nutrisiA: currConf.nutrientA,
            nutrisiB: currConf.nutrientB,
            lamp_uv_enable: currConf.colorSwitch,
            lamp_grow_enable: currConf.whiteSwitch,
            lamp_grow_timer: currConf.whiteTimeList,
            lamp_uv_timer: currConf.colorTimeList,
            auto_pilot: this.state.autoPilot,
            main_recipe: this.state.projectSelection.recipeID,
            active_recipe: null,
        }

        const updateOK = await updateConfig(newConfig, this.state.projectSelection.serialNumber);

        if(updateOK){
            message = 'Successfully removed reference recipe';
        }

        this.setState({ referenceRecipe: null, });
        this.props.stopLoading();
        this.props.openSnackBar(message);
    }

    handleAddReferenceRecipe(newReferenceRecipe) {
        this.setState({
            referenceRecipe: newReferenceRecipe,
        });
    }

    handleAutoPilotChange() {
        //Inverts the previous state
        this.setState((state, props) => ({
            autoPilot: !state.autoPilot,
        }), () => {
            if (this.state.autoPilot) {
                //TODO: Fetch Reference Recipe Configuration
            }
        });
    }

    checkAutoPilot(isAutoPilot) {
        if (this.state.referenceRecipe !== null) {
            this.setState({
                autoPilot: isAutoPilot,
            });
        }
    }

    setConfigID(configID) {
        this.setState({
            configID: configID,
        });
    }

    wrapperGetConfig(){
        return this.refs.monitorPage.getConfigState();
    }

    /* End of Front End Logic Methods */
    
    /* UI Methods */

    updateDimensions() {
        this.setState({
            drawerWidth: window.innerWidth > 960 ? 225 : 0,
        });
    }
    
    handleAvatarClick(e){
        this.setState({
            userMenuRef: e.currentTarget,
        });
    }

    handleCloseUserMenu(){
        this.setState({
            userMenuRef: null,
        })
    }

    handleOpenMobileDrawer() {
        this.setState((state, props) => ({
            openMobileDrawer: !state.openMobileDrawer,
        }));
    }

    /* End Of UI Methods */

    /* Dialog Methods */

    handleOpenDialog(dialogName){
        this.setState({
            [dialogName] : true,
        });
    }

    handleCloseDialog(dialogName){
        let newState = null;

        if(dialogName === 'closeNewRecipeDialog'){
            newState = {
                projectSelection:null,
                openNewRecipeDialog: false,
            }
        } else {
            newState = {
                [dialogName]: false,
            }
        }
        
        this.setState(newState);
    }

    handleNewRecipe(device) {
        this.setState({
            projectSelection: {
                deviceID: device.deviceID,
                serialNumber: device.serialNumber,
                deviceName: device.deviceName,
                recipeID: null,
                recipeName: null,
                recipeStart: null,
            },
            openNewRecipeDialog: true,
        });
    }

    handleAddDevice() {
        this.setState({
            openNewDeviceDialog: true,
        })
    }

    handleHarvest() {
        if (this.state.projectSelection !== null && this.state.projectSelection.recipeID !== '') {
            this.setState({
                openHarvestDialog: true,
            });
        } else {
            this.props.openSnackBar('Please choose a recipe first!');
        }
    }

    /* End of Dialog Methods */

    render() {
        const appBarHeight = "84px";

        if(localStorage.getItem('AuthToken') === null){
            return <Redirect to="/login" />
        } else{
            return (
                <div style={{ backgroundColor: 'gray'}}>
                    <Hidden smDown>
                        <Drawer variant="permanent" anchor="left" width={this.state.drawerWidth + 'px'}>
                            <List>
                                {
                                    this.drawerLinks.map(item => {
                                        if (item.link === null) {
                                            return (
                                                <ListItem key={item.key} alignItems="center">
                                                    <ListItemText primary={item.component} />
                                                </ListItem>
                                            );
                                        }
                                        else {
                                            return (
                                                <ListItem key={item.key} alignItems="center" component={NavLink} exact to={item.link} activeClassName='Mui-selected'>
                                                    <ListItemText primary={item.component} />
                                                </ListItem>
                                            );
                                        }
                                    })
                                }
                            </List>
                        </Drawer>
                    </Hidden>
                    <Hidden mdUp>
                        <Drawer variant="temporary" anchor="left" open={this.state.openMobileDrawer} onClose={this.handleOpenMobileDrawer}>
                            <List style={{ paddingTop: appBarHeight }}>
                                {
                                    this.drawerLinks.map(item => {
                                        if (item.link === null) {
                                            if (item.icon === null) {
                                                return null;
                                            } else {
                                                return (
                                                    <ListItem key={item.key} alignItems="center">
                                                        <ListItemText primary={item.icon} />
                                                    </ListItem>
                                                );
                                            }
                                        }
                                        else {
                                            return (
                                                <ListItem key={item.key} alignItems="center" component={NavLink} exact to={item.link} activeClassName='Mui-selected'>
                                                    <ListItemText primary={item.icon} />                                                    
                                                </ListItem>
                                            );
                                        }
                                    })
                                }
                            </List>
                        </Drawer>
                    </Hidden>
                    <AppBar position='static' style={{height: appBarHeight, paddingLeft:this.state.drawerWidth + 'px'}}>
                        <Toolbar>
                            <Hidden mdUp>
                                <IconButton onClick={this.handleOpenMobileDrawer} edge='start' style={{paddingLeft:'-16px', marginTop:'12px'}} size='medium'>
                                    <Menu edge='start'/>
                                </IconButton>
                            </Hidden>
                            <Box style={{ flexGrow: '1', textOverflow:'ellipsis', height:'64px' }}>
                                <TextField select
                                    margin='dense'
                                    style={{outlineColor:'white' }}
                                    onChange={e => {this.handleProjectChange(e.target.value)}}
                                    value={this.state.projectSelection === null ? '' : this.state.projectSelection}
                                    SelectProps={{
                                        displayEmpty:true,
                                        renderValue: projectSelection => {
                                            const primaryText = projectSelection === '' ? 'Please Choose a Recipe' : projectSelection.recipeName;
                                            const secondaryText = projectSelection === '' ? '' : projectSelection.deviceName;
                                            
                                            return <ListItemText primary={primaryText} secondary={secondaryText} primaryTypographyProps={{style:{color: 'white'}}} secondaryTypographyProps={{style:{color:'white'}}}/>
                                        },
                                    }}
                                >
                                    {
                                        this.state.deviceList.map(device => (
                                            device.recipeID !== null ? (
                                                <MenuItem value={{ deviceID: device.deviceID, deviceName: device.deviceName, serialNumber: device.serialNumber, recipeName: device.recipeName, recipeID: device.recipeID, recipeStart:device.recipeStart }} key={device.serialNumber} component={ListItem}> 
                                                    <ListItemText primary={device.recipeName} secondary={device.deviceName} />
                                                </MenuItem>
                                            ) : null
                                        ))
                                    }
                                </TextField>
                            </Box>
                            <IconButton edge='end' onClick={this.handleAvatarClick} aria-describedby={Boolean(this.state.userMenuRef) ? 'simple-popover' : undefined}>
                                <Avatar style={{ marginTop: '12px', backgroundColor: 'darkblue' }}>
                                    {this.props.user.name !== '' ? this.props.user.name.split(' ', 2).map(name => { return name.slice(0, 1) }) : 'EKA'}
                                </Avatar>
                            </IconButton>
                            <Popover
                                id={Boolean(this.state.userMenuRef) ? 'simple-popover' : undefined}
                                open={Boolean(this.state.userMenuRef)}
                                onClose={this.handleCloseUserMenu}
                                anchorEl={this.state.userMenuRef}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'right',
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                            >
                                <Paper>
                                    <ClickAwayListener onClickAway={this.handleCloseUserMenu}>
                                        <MenuList autoFocusItem={Boolean(this.state.userMenuRef)}>
                                            <MenuItem onClick={this.props.logOut}> Log Out </MenuItem>
                                        </MenuList>
                                    </ClickAwayListener>
                                </Paper>
                            </Popover>
                        </Toolbar>
                    </AppBar>
                    <Dialogs 
                        configID={this.state.configID}
                        autoPilot={this.state.autoPilot}
                        projectSelection={this.state.projectSelection} 
                        openHarvestDialog={this.state.openHarvestDialog}
                        openNewRecipeDialog={this.state.openNewRecipeDialog}
                        openNewDeviceDialog={this.state.openNewDeviceDialog}
                        openReferRecipeDialog={this.state.openReferRecipeDialog}

                        stopLoading={this.props.stopLoading}
                        openSnackBar={this.props.openSnackBar}
                        startLoading={this.props.startLoading}

                        closeDialog={this.handleCloseDialog}
                        fetchDeviceList={this.fetchDeviceList}
                        handleProjectChange={this.handleProjectChange}
                        getCurrentConfiguration={this.wrapperGetConfig}
                        handleAddReferenceRecipe={this.handleAddReferenceRecipe}
                    />
                    <Box style={{paddingLeft: `${this.state.drawerWidth + 8}px`, paddingTop:'16px', paddingRight:'8px', height:'100vh', overflow: 'auto' }}>
                        <Route exact path={this.props.match.path + '/:page'}
                            render={props => <StatusBar {...props}
                                    editMode={this.state.editMode}
                                    autoPilot={this.state.autoPilot}
                                    recipe={this.state.referenceRecipe}
                                    handleAddDevice={this.handleAddDevice}
                                    handleModeChange={this.handleModeChange}
                                    handleAutoPilotChange={this.handleAutoPilotChange}
                                    handleRemoveReferenceRecipe={this.handleRemoveReferenceRecipe}
                                    handleAddReferenceRecipe={() => {this.handleOpenDialog('openReferRecipeDialog')}}
                                    initTimestamp={this.state.projectSelection === null ? 0 : this.state.projectSelection.recipeStart}
                                />}
                        />
                        <Box mt={2}>
                            <Switch>
                                <Route exact path={this.props.match.path + '/devices'}>
                                    <DevicesPage deviceList={this.state.deviceList} handleProjectChange={this.handleProjectChange} handleNewRecipe={this.handleNewRecipe}/>
                                </Route>
                                {
                                    this.state.projectSelection === null ? <Redirect push to={this.props.match.path + '/devices'} /> :
                                    <React.Fragment>
                                        <Route exact path={this.props.match.path + '/records'} render={props => <RecordsPage {...props} projectSelection={this.state.projectSelection} startLoading={this.props.startLoading} stopLoading={this.props.stopLoading} fetchRecords={this.getRecords} recordsList={this.state.recordsList} />} />
                                        <Route exact path={this.props.match.path + '/photos'} render={props => <PhotosPage  {...props} projectSelection={this.state.projectSelection} startLoading={this.props.startLoading} stopLoading={this.props.stopLoading} fetchPhotos={this.getPhotos} photoList={this.state.photoList} />} />
                                        <Route exact path={this.props.match.path + '/monitor'}>
                                            <MonitorPage ref='monitorPage' setConfigID={this.setConfigID} configID={this.state.configID} startLoading={this.props.startLoading} stopLoading={this.props.stopLoading} openSnackBar={this.props.openSnackBar} projectSelection={this.state.projectSelection} autoPilot={this.state.autoPilot} editMode={this.state.editMode} referenceRecipe={this.state.referenceRecipe} checkAutopilot={this.checkAutoPilot}/>
                                        </Route>
                                    </React.Fragment>
                                }
                            </Switch>
                        </Box>
                    </Box>
                </div>
            );
        }
    }
}

export default Dashboard;