import { AcUnit, Brightness5, Colorize, Opacity, Waves, WbIncandescent } from '@material-ui/icons';
import { Grid, InputAdornment, TextField, Typography } from '@material-ui/core';
import { areIntervalsOverlapping, isBefore, lightFormat } from 'date-fns';
import Paho from "paho.mqtt.js";
import React from 'react';
import BigDisplay from '../../components/Components_Dashboard/BigDisplay';
import LightSettings from '../../components/Components_Dashboard/LightSettings';
import SmallDisplay from '../../components/Components_Dashboard/SmallDisplay';
import PhotoCard from '../../components/Components_Dashboard/PhotoCard';
import {updateConfig, fetchMeasurements, fetchConfiguration, fetchLatestPhoto} from '../../Logic/Logic_Dashboard/DevicesLogic';

/**
 * !Props:
 * * Variables
 * autoPilot        : Boolean, represents whether the device is in Auto Pilot mode or not
 * editMode         : Boolean, represents the currently active UI Mode the user is interacting with
 * configID         : Number/String, represents the ID of the currently running configuration for the active recipe
 * projectSelection : JSObject, contains information about the currently chosen device and recipe
 * referenceRecipe  : JSObject, contains information about the currently referenced "reference recipe", if any
 * 
 * * Methods
 * openSnackBar: HOC Method to display a message via SnackBar
 * startLoading: HOC Method to display LoadingOverlay
 * stopLoading : HOC Method to hide LoadingOverlay
 * checkAutopilot: Method for changing the HOC autoPilot state based on fetched configuration
 * setConfigID   : Method for setting the HOC configID based on fetched configuration
 * 
 * !States:
 * measurement      : JSObject, contains all measurements/monitored data
 * configuration    : JSObject, contains all configuration data
 * recentPhotoLink  : String, used to load the latest photo for the recipe
 */

class MonitorPage extends React.Component {
    mqttClient = null;
    disableButtons = true;
    timePrecedenceErrorMessage = 'Waktu mulai harus sebelum waktu selesai!';
    overlappingTimeErrorMessage = 'Sudah ada waktu yang mencakup interval waktu ini!';

    constructor(props) {
        super(props);

        this.getConfigState = this.getConfigState.bind(this);
        this.fetchConfig = this.fetchConfig.bind(this);
        this.postConfiguration = this.postConfiguration.bind(this);

        this.handleMQTTConnectionEstablished = this.handleMQTTConnectionEstablished.bind(this);
        this.handleMQTTMessageArrived = this.handleMQTTMessageArrived.bind(this);
        this.handleMQTTConnectionLost = this.handleMQTTConnectionLost.bind(this);
        this.handleMQTTConnectionFailed = this.handleMQTTConnectionFailed.bind(this);
        this.sendCameraCommand = this.sendCameraCommand.bind(this);

        this.handleColorTimeSwitchChange = this.handleColorTimeSwitchChange.bind(this);
        this.handleWhiteTimeSwitchChange = this.handleWhiteTimeSwitchChange.bind(this);
        this.handleColorTimeListRemove = this.handleColorTimeListRemove.bind(this);
        this.handleWhiteTimeListRemove = this.handleWhiteTimeListRemove.bind(this);
        this.handleColorTimeListAdd = this.handleColorTimeListAdd.bind(this);
        this.handleWhiteTimeListAdd = this.handleWhiteTimeListAdd.bind(this);
        this.handleNutrientAChange = this.handleNutrientAChange.bind(this);
        this.handleNutrientBChange = this.handleNutrientBChange.bind(this);
        this.handleTDSConfigChange = this.handleTDSConfigChange.bind(this);
        this.handleConfigChange = this.handleConfigChange.bind(this);

        this.state = {
            measurement: {
                tds: 0,
                lux: 0,
                humidity: 0,
                temperature: 0,
            },
            configuration: {
                tds: 0,
                nutrientA: 0,
                nutrientB: 0,
                colorSwitch: false,
                whiteSwitch: false,
                colorTimeList: [],
                whiteTimeList: [],
            },
            recentPhotoLink: '',
        }

        fetchLatestPhoto(this.props.projectSelection.serialNumber).then(link => {
            this.state.recentPhotoLink = link
        });
    }

    getConfigState(){
        return {
            tds: this.state.configuration.tds,
            nutrientA: this.state.configuration.nutrientA,
            nutrientB: this.state.configuration.nutrientB,
            colorSwitch: this.state.configuration.colorSwitch,
            whiteSwitch: this.state.configuration.whiteSwitch,
            colorTimeList: Array.from(this.state.configuration.colorTimeList),
            whiteTimeList: Array.from(this.state.configuration.whiteTimeList),
        };
    }   

    /* End of Front End Logic Methods */

    async fetchConfig(){
        this.props.startLoading();
        const config = await fetchConfiguration(this.props.projectSelection.serialNumber);

        let stateConfig = {
            tds: 0,
            nutrientA: 0,
            nutrientB: 0,
            colorSwitch: false,
            whiteSwitch: false,
            colorTimeList: [],
            whiteTimeList: [],
        }

        if(config !== null){
            stateConfig = {
                tds: config.tds_value,
                nutrientA: config.nutrisiA,
                nutrientB: config.nutrisiB,
                colorSwitch: config.lamp_uv_enable,
                whiteSwitch: config.lamp_grow_enable,
                colorTimeList: config.lamp_uv_timer,
                whiteTimeList: config.lamp_grow_timer,
            }

            this.props.checkAutopilot(config.auto_pilot);
            this.props.setConfigID(config.id);
        }

        await this.handleConfigChange(stateConfig);
        await fetchLatestPhoto(this.props.projectSelection.serialNumber);
        this.props.stopLoading();
    }

    async postConfiguration(){
        this.props.startLoading();
        const parameter = {
            id: this.props.configID,
            tds_value: this.state.configuration.tds,
            nutrisiA: this.state.configuration.nutrientA,
            nutrisiB: this.state.configuration.nutrientB,
            lamp_uv_enable: this.state.configuration.colorSwitch,
            lamp_grow_enable: this.state.configuration.whiteSwitch,
            lamp_grow_timer: this.state.configuration.whiteTimeList,
            lamp_uv_timer: this.state.configuration.colorTimeList,
            auto_pilot: this.props.autoPilot,
            main_recipe:this.props.projectSelection.recipeID,
            active_recipe: this.props.referenceRecipe === null ? null : this.props.referenceRecipe.recipeID,
        }

        const success = await updateConfig(parameter);
        const msgString = success ? 'Successfully updated configuration!' : 'Failed to update! Reverting Changes';
        this.props.openSnackBar(msgString);
        this.props.stopLoading();
        return success;
    }

    /* End of Front End Logic Methods */

    /* MQTT Related Methods */
    async componentDidMount() {
        const portNum = 443;
        const brokerUrl = 'mqtt.flespi.io';
        const clientID = 'FL0R1D4||client||' + this.props.projectSelection.serialNumber;
        const username = 'FlespiToken l86MbAVQJxNctBSP8ZMLVjs7fWzc0qrbnnOoLx3kVEmc52S1RsABzQwEDVGt9Vsm';

        this.mqttClient = new Paho.Client(brokerUrl, Number(portNum), '/mqtt', clientID);
        this.mqttClient.onConnectionLost = this.handleMQTTConnectionLost;
        this.mqttClient.onMessageArrived = this.handleMQTTMessageArrived;
        this.mqttClient.connect({
            userName: username,
            password: '',
            onSuccess: this.handleMQTTConnectionEstablished,
            onFailure: this.handleMQTTConnectionFailed,
            mqttVersion: 3,
            useSSL: true,
        });

        await this.fetchConfig();
    } 

    handleMQTTChangeTopic(){
        const topic = 'FL0R1D4/' + this.props.projectSelection.serialNumber;
        try {
            this.mqttClient.unsubscribe(topic);
        } catch (error){
            this.props.openSnackBar('An Error Occured! Please refresh the browser to try again!');
        }
    }

    handleMQTTConnectionEstablished(){
        console.log("Connection to MQTT Broker Successful!");
        const topic = 'FL0R1D4/' + this.props.projectSelection.serialNumber;

        try {
            this.mqttClient.subscribe(topic, {
                qos: 1,
                onSuccess: async resp => {
                    this.props.openSnackBar('Successfully Connected to MQTT Broker! Fetching latest measurement...');
                    const measurement = await fetchMeasurements(this.props.projectSelection.deviceID);

                    if(measurement === null){
                        this.props.openSnackBar('Failed to get measurement!');
                        this.setState({
                            disableButtons: true,
                        });
                    } else {
                        this.props.openSnackBar('Measurements updated');
                        this.setState({
                            disableButtons: false,
                            measurement: measurement
                        });
                    }
                },
                onFailure: resp => {
                    this.setState({
                        disableButtons: true,
                    })
                    console.log('Error Code : ' + resp.errorCode);
                    console.log('Error Message : ');
                    console.log(resp.errorMessage);

                    this.props.openSnackBar('Failed to subscribe to MQTT Topic! Check your internet connection and refresh the browser to try again!');
                }
            });
        } catch (error) {
            this.props.openSnackBar('Not connected to MQTT Broker! Check your internet connection and try again!');
            this.setState({
                disableButtons: true,
            });
        }
    }

    handleMQTTConnectionFailed(responseObj){
        console.log(responseObj);
        this.props.openSnackBar('Connection to Server failed! Please reload the page to try again!');
        this.setState({
            disableButtons: true,
        })
    }

    handleMQTTConnectionLost(responseObj){
        this.setState({
            disableButtons: true,
        })

        if(responseObj.errorCode !== 0){
            console.log(responseObj.errorMessage);
            this.props.openSnackBar('Connection to Server lost! Please reload the page to reconnect!');
        }
    }

    async handleMQTTMessageArrived(msg){
        if(!msg.duplicate){
            if(msg.payloadString === 'fetch_monitor'){
                const measurement = await fetchMeasurements(this.props.projectSelection.deviceID);
                if (measurement === null) {
                    this.props.openSnackBar('Error Getting Measurements!');
                } else {
                    this.props.openSnackBar('Measurements');
                    this.setState({
                        measurement: measurement,
                    });
                }
            } else if (msg.payloadString === 'fetch_photo'){
                const imageLink = await fetchLatestPhoto(this.props.projectSelection.serialNumber);
                if(imageLink === null){
                    this.props.openSnackBar('Failed to fetch latest photo!');
                } else {
                    this.props.openSnackBar('Updated Latest Photos!');
                    this.setState({
                        recentPhotoLink: imageLink,
                    })
                }
            } else if(msg.payloadString === 'fetch_conf_auto' && this.props.autoPilot){
                await this.fetchConfig();
            }
        }
    }

    sendCameraCommand() {
        this.props.startLoading();
        const topic = 'FL0R1D4/' + this.props.projectSelection.serialNumber;
        let mqttMessage = new Paho.Message("take_photo");
        mqttMessage.destinationName = topic;
        mqttMessage.qos = 1;
        this.mqttClient.send(mqttMessage);
        this.props.stopLoading();
    }

    componentWillUnmount() {
        this.setState({
            disableButtons: true,
        })
        this.mqttClient.disconnect();
    }
    /* End Of MQTT Related Methods */

    /* Input Handling Section */

    handleColorTimeSwitchChange(){
        const configuration = {
            ...this.state.configuration,
            colorSwitch: !this.state.configuration.colorSwitch,
        }

        this.handleConfigChange(configuration);
    }

    handleWhiteTimeSwitchChange(){
        const configuration = {
            ...this.state.configuration,
            whiteSwitch: !this.state.configuration.whiteSwitch,
        }

        this.handleConfigChange(configuration);
    }

    handleColorTimeListRemove(idx){
        let currArr = this.state.configuration.colorTimeList;

        currArr[idx] = {
            id: currArr[idx].id,
            start: "00:00:00",
            end: "00:00:00",
        }

        const configuration= {
            ...this.state.configuration,
            colorTimeList: currArr,
        }
        this.handleConfigChange(configuration);
    }

    handleWhiteTimeListRemove(idx) {
        let currArr = this.state.configuration.whiteTimeList;
        
        currArr[idx] = {
            id: currArr[idx].id,
            start: "00:00:00",
            end: "00:00:00",
        }

        const configuration= {
            ...this.state.configuration,
            whiteTimeList: currArr,
        }
        this.handleConfigChange(configuration);
    }

    handleColorTimeListAdd(newTime){
        if (isBefore(newTime.start, newTime.end)) {
            let overlapping = false;
            let currArr = this.state.configuration.colorTimeList;
            let newConfig = null;
            const now = new Date();

            for(const interval of currArr){
                if(interval.start === interval.end){
                    continue;
                }

                const timeArrStart = interval.start.split(':');
                const timeArrEnd = interval.end.split(':');
                if(areIntervalsOverlapping({
                    start: new Date(now.getFullYear(), now.getMonth(), now.getDate(), parseInt(timeArrStart[0], 10), parseInt(timeArrStart[1], 10), parseInt(timeArrStart[2],10)),
                    end: new Date(now.getFullYear(), now.getMonth(), now.getDate(), parseInt(timeArrEnd[0], 10), parseInt(timeArrEnd[1], 10), parseInt(timeArrEnd[2],10)),
                }, newTime)){
                    overlapping = true;
                    break;
                }
            }

            if(overlapping){
                this.props.openSnackBar(this.overlappingTimeErrorMessage);
            } else {

                if (currArr.some(val => val.start === val.end)){
                    const idx = currArr.findIndex(val => val.start === val.end);
                    
                    const startTimestamp =  lightFormat(newTime.start, 'HH:mm:ss');
                    const endTimestamp =  lightFormat(newTime.end, 'HH:mm:ss');

                    currArr[idx] = {
                        id : currArr[idx].id,
                        start : startTimestamp,
                        end: endTimestamp,
                    }

                    currArr.sort();
                    newConfig = {
                        ...this.state.configuration,
                        colorTimeList: currArr,
                    }
                } else {
                    newConfig = this.state.configuration;
                }

                this.handleConfigChange(newConfig);
            }
        } else {
            this.props.openSnackBar(this.timePrecedenceErrorMessage);
        }
    }

    handleWhiteTimeListAdd(newTime){
        if(isBefore(newTime.start, newTime.end)){
            let overlapping = false;
            let currArr = this.state.configuration.whiteTimeList;
            let newConfig = null;
            const now = new Date();

            for (const interval of currArr) {
                if(interval.start === interval.end){
                    continue;
                }

                const timeArrStart = interval.start.split(':');
                const timeArrEnd = interval.end.split(':');
                if (areIntervalsOverlapping({
                    start: new Date(now.getFullYear(), now.getMonth(), now.getDate(), parseInt(timeArrStart[0], 10), parseInt(timeArrStart[1], 10), parseInt(timeArrStart[2], 10)),
                    end: new Date(now.getFullYear(), now.getMonth(), now.getDate(), parseInt(timeArrEnd[0], 10), parseInt(timeArrEnd[1], 10), parseInt(timeArrEnd[2], 10)),
                }, newTime)) {
                    overlapping = true;
                    break;
                }
            }

            if (overlapping) {
                this.props.openSnackBar(this.overlappingTimeErrorMessage);
            } else {
                
                if (currArr.some(val => val.start === val.end)) {
                    const idx = currArr.findIndex(val => val.start === val.end);

                    const startTimestamp = lightFormat(newTime.start, 'HH:mm:ss');
                    const endTimestamp = lightFormat(newTime.end, 'HH:mm:ss');

                    currArr[idx] = {
                        id: currArr[idx].id,
                        start: startTimestamp,
                        end: endTimestamp,
                    }

                    currArr.sort();
                    newConfig = {
                        ...this.state.configuration,
                        whiteTimeList: currArr,
                    }
                } else {
                    newConfig = this.state.configuration;
                }

                this.handleConfigChange(newConfig);
            }
        } else {
            this.props.openSnackBar(this.timePrecedenceErrorMessage);
        }
    }

    handleNutrientAChange(e){
        const configuration = {
            ...this.state.configuration,
            nutrientA: e.target.value,
        }

        this.handleConfigChange(configuration);
    }

    handleNutrientBChange(e){
        const configuration = {
            ...this.state.configuration,
            nutrientB: e.target.value,
        }

        this.handleConfigChange(configuration);
    }

    handleTDSConfigChange(e){
        const configuration = {
            ...this.state.configuration,
            tds: e.target.value,
        }

        this.handleConfigChange(configuration);
    }

    async handleConfigChange(newConfig){
        this.setState({configuration: newConfig});
    }

    /* End of Input Handling Section */

    render() {
        return (
            <Grid container direction="row" alignItems="flex-start" justify="flex-start" spacing={1}>
                <SmallDisplay
                    avatar={<Opacity/>}
                    title='TDS'
                    referenceValue={this.props.referenceRecipe === null ? null : this.props.referenceRecipe.data.tds.toFixed(2)}
                    unitOfMeasurement='ppm'
                >
                    <TextField
                        disabled={!(!this.props.autoPilot && this.props.editMode)}
                        fullWidth
                        variant='outlined'
                        name='tds'
                        value={this.state.configuration.tds}
                        type='number'
                        margin='dense'
                        onChange={this.handleTDSConfigChange}
                        inputProps={{
                            max: 10000,
                            min: 0,
                            step: 10,
                        }}
                        InputProps={{
                            endAdornment: <InputAdornment position="end"> <Typography variant="overline"> ppm </Typography> </InputAdornment>,
                        }}
                    />
                    <Grid item xs={6} style={{marginTop:'16px'}}>   
                        <Typography variant='subtitle2'> Measured </Typography>
                        <Typography variant='h5'> {this.state.measurement.tds.toFixed(2)} </Typography>
                        <Typography variant='overline'> ppm </Typography>
                    </Grid>
                </SmallDisplay>

                <SmallDisplay
                    avatar={<Colorize/>}
                    title='Nutrients Ratio'
                    referenceValue={this.props.referenceRecipe === null ? null : this.props.referenceRecipe.data.nutrients}
                    unitOfMeasurement='A:B'
                >
                    <TextField
                        disabled={!(!this.props.autoPilot && this.props.editMode)}
                        variant='outlined'
                        type='number'
                        name='nutrientA'
                        margin='dense'
                        value={this.state.configuration.nutrientA}
                        onChange={this.handleNutrientAChange}
                        inputProps={{
                            min: 0,
                            max: 9,
                            step: 1,
                        }}
                        InputProps={{
                            endAdornment: <InputAdornment position="end"> A </InputAdornment>,
                        }}
                        style={{
                            marginRight: '5%',
                            width: '45%'
                        }}
                    />
                    <TextField
                        disabled={(!this.props.autoPilot && this.props.editMode) ? false : true}
                        variant='outlined'
                        type='number'
                        name='nutrientB'
                        margin='dense'
                        value={this.state.configuration.nutrientB}
                        onChange={this.handleNutrientBChange}
                        inputProps={{
                            min: 0,
                            max: 9,
                            step: 1,
                        }}
                        InputProps={{
                            endAdornment: <InputAdornment position="end"> B </InputAdornment>,
                        }}
                        style={{
                            marginLeft: '5%',
                            width: '45%',
                        }}
                    />
                </SmallDisplay>
                    
                <BigDisplay
                    avatar={<WbIncandescent color='secondary'/>}
                    title='Red/Blue Lights'
                >
                    <LightSettings onColor='magenta' color='secondary' handleSwitchChange={this.handleColorTimeSwitchChange} isChecked={this.state.configuration.colorSwitch} timeListAdd={this.handleColorTimeListAdd} timeListRemove={this.handleColorTimeListRemove} timeList={this.state.configuration.colorTimeList} autoPilot={this.props.autoPilot} editMode={this.props.editMode} />
                </BigDisplay>
                <BigDisplay
                    avatar={<WbIncandescent />}
                    title='White Lights'
                >
                    <LightSettings onColor='yellow' color='primary' handleSwitchChange={this.handleWhiteTimeSwitchChange} isChecked={this.state.configuration.whiteSwitch} timeList={this.state.configuration.whiteTimeList} autoPilot={this.props.autoPilot} editMode={this.props.editMode} timeListAdd={this.handleWhiteTimeListAdd} timeListRemove={this.handleWhiteTimeListRemove}/>
                </BigDisplay>
                <SmallDisplay
                    avatar={<Brightness5 />}
                    title='Illuminance'
                    referenceValue={this.props.referenceRecipe === null ? null : this.props.referenceRecipe.data.illuminance.toFixed(2)}
                    unitOfMeasurement='lux'
                >
                    <Grid item container direction='row' xs={12}>
                        <Grid item xs={12}>
                            <Typography variant='h4'> {this.state.measurement.lux.toFixed(2)} </Typography> 
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant='overline'> lux </Typography>
                        </Grid>
                    </Grid> 
                </SmallDisplay>
                <SmallDisplay
                    avatar={<AcUnit />}
                    title='Temperature'
                    referenceValue={this.props.referenceRecipe === null ? null : this.props.referenceRecipe.data.temperature.toFixed(2)}
                    unitOfMeasurement='&deg;C'
                >
                    <Grid item container direction='row' xs={12}>
                        <Grid item xs={12}>
                            <Typography variant='h4'> {this.state.measurement.temperature.toFixed(2)} </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant='overline'> &deg;C </Typography>
                        </Grid>
                    </Grid>
                </SmallDisplay>
                <SmallDisplay
                    avatar={<Waves />}
                    title='Humidity'
                    referenceValue={this.props.referenceRecipe === null ? null : this.props.referenceRecipe.data.humidity.toFixed(2)}
                    unitOfMeasurement='%'
                >
                    <Grid item container direction='row' xs={12}>
                        <Grid item xs={12}>
                            <Typography variant='h4'> {this.state.measurement.humidity.toFixed(2)} </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant='overline'> % </Typography>
                        </Grid>
                    </Grid>
                </SmallDisplay>
                <PhotoCard sendCameraCommand={this.sendCameraCommand} disabled={this.state.disableButtons} imageLink={this.state.recentPhotoLink}/>
            </Grid>
        );
    }
}

export default MonitorPage;