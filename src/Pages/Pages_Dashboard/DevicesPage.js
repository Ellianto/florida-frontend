import { Button, Card, CardContent, CardHeader, CardMedia, Container, Grid, Typography } from '@material-ui/core';
import React from 'react';

/**
 * !Props
 * * Variables
 * deviceList   : Array<JSObject>, represents the list of "logical devices" of the current user
 * 
 * * Methods
 * handleProjectChange  : Method for changing HOC's projectSelection state
 * handleNewRecipe      : Method for displaying "Create New Recipe" Dialog
 */

class DevicesPage extends React.Component {
    formatTime(timestamp) {
        const timeDiff = (new Date()).valueOf() - timestamp;

        const dayDiff = Math.floor(timeDiff / (24 * 60 * 60 * 1000));
        const hourDiff = Math.floor((timeDiff % (24 * 60 * 60 * 1000)) / (60 * 60 * 1000));
        const minuteDiff = Math.floor((timeDiff % (60 * 60 * 1000)) / (60 * 1000));
        const secondDiff = Math.floor((timeDiff % (60 * 1000)) / (1000));

        return dayDiff + ' day ' + (dayDiff > 1 ? 's' : '') + hourDiff + ':' + minuteDiff + ':' + secondDiff;
    }

    render() {
        return (
            <Grid container direction='row' justify='flex-start' alignItems='flex-start' spacing={1} >
                {
                    this.props.deviceList.map(device => (
                        <Grid item xs={12} sm={6} md={4} lg={3} key={device.serialNumber}>
                            <Card onClick={device.recipeName === null ? () => {this.props.handleNewRecipe(device)} : () => { this.props.handleProjectChange(device)}}>
                                <CardHeader title={device.deviceName} subheader={device.serialNumber} />
                                <CardContent>
                                    {
                                        device.recipeID === null ?
                                        <Container style={{ height: '400px', paddingTop: '192px', backgroundColor: 'lightgray', borderRadius: '4px' }}>
                                            <Button variant="contained" color="primary" fullWidth> Start New Recipe </Button>
                                        </Container>
                                        :
                                        <Card style={{height:'400px'}} elevation={3}>
                                            <CardMedia component="img" src={device.imageLink} title={device.deviceName} alt={device.deviceName} />
                                            <CardContent>
                                                <Typography variant="h6"> {device.recipeName} </Typography>
                                                <Typography variant="body1"> Plant's Age </Typography>
                                                <Typography variant="overline"> {this.formatTime(device.recipeStart)} </Typography>
                                            </CardContent>
                                        </Card>
                                    }
                                </CardContent>
                            </Card>
                        </Grid>
                    ))
                }
            </Grid>
        );
    }
}

export default DevicesPage;