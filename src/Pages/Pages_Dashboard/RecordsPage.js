import React from 'react';
import {Table, TableHead, TableBody, TableRow, TableCell, Paper, Typography} from '@material-ui/core';

/**
 * !Props
 * * Variables
 * projectSelection : JSObject, represents the information about the currently chosen active recipe
 * recordsList      : Array<JSObject>, contains information about the records (configuration & monitor data) of the currentlty chosen active recipe
 * 
 * * Methods
 * startLoading: HOC Methods to display LoadingOverlay
 * stopLoading : HOC Methods to hide LoadingOverlay
 * fetchRecords: HOC Methods to fetch the Record List. Passed so that it can me done only when the component mounts
 */

class RecordsPage extends React.Component {
    async componentDidMount() {
        await this.props.fetchRecords();
    }

    formatTime(age) {
        const timestamp = Math.floor(age);
        const secondDiff = Math.floor(timestamp % (60));
        const minuteDiff = Math.floor((timestamp % (60 * 60)) / 60);
        const hourDiff = Math.floor((timestamp % (24 * 60 * 60)) / (60 * 60));
        const dayDiff = Math.floor(timestamp / (24 * 60 * 60));

        const dayString = String(dayDiff) + ' day ' + (dayDiff > 1 ? 's' : '');
        const hourString = String(hourDiff) + ' hr ';
        const minuteString = String(minuteDiff) + ' min ';
        const secondString = String(secondDiff) + ' sec ';

        return (
            <Typography variant='subtitle2'>
                {dayString} 
                {hourString}
                {minuteString} 
                {secondString}
            </Typography>
        )
    }

    render() {
        return (
            <Paper style={{width:'100%', overflowX:'auto'}}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell> ID </TableCell>
                            <TableCell> Timestamp </TableCell>
                            <TableCell> TDS (Set/Measured) </TableCell>
                            <TableCell> Nutrients (A:B) </TableCell>
                            <TableCell> Red/Blue Lights </TableCell>
                            <TableCell> White Lights </TableCell>
                            <TableCell> Temperature </TableCell>
                            <TableCell> Humidity </TableCell>
                            <TableCell> Illuminance </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            this.props.recordsList.length > 0 ?
                            this.props.recordsList.map((record) => (
                                <TableRow key={record.id}>
                                    <TableCell> {record.id} </TableCell>
                                    <TableCell> {this.formatTime(record.age)} </TableCell>
                                    <TableCell> {record.tdsSet} / {record.tdsMeasure.toFixed(2)} ppm </TableCell>
                                    <TableCell> {record.nutrientsA} : {record.nutrientsB} </TableCell>
                                    <TableCell> {record.colorOn ? 'ON' : 'OFF'} </TableCell>
                                    <TableCell> {record.whiteOn ? 'ON' : 'OFF'} </TableCell>
                                    <TableCell> {record.temperature.toFixed(2)}&deg;C </TableCell>
                                    <TableCell> {record.humidity.toFixed(2)}% </TableCell>
                                    <TableCell> {record.illuminance.toFixed(2)} lux </TableCell>
                                </TableRow>
                            ))
                            :
                            null
                        }
                    </TableBody>
                </Table>
            </Paper>
        );
    }
}

export default RecordsPage;