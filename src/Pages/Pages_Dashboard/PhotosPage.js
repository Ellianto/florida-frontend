import React from 'react';
import {Grid, Card, CardMedia, CardHeader} from '@material-ui/core';


/**
 * !Props
 * * Variables
 * projectSelection : JSObject, represents the information about the currently chosen active recipe
 * photoList        : Array<JSObject>, contains information about the photos of the currentlty chosen active recipe
 * 
 * * Methods
 * startLoading: HOC Methods to display LoadingOverlay
 * stopLoading : HOC Methods to hide LoadingOverlay
 * fetchPhotos: HOC Methods to fetch the Photo List. Passed so that it can me done only when the component mounts
 */

class PhotosPage extends React.Component {
    componentDidMount(){
        this.props.fetchPhotos();
    }

    formatTime(timestamp) {
        const dayDiff = Math.floor(timestamp / (24 * 60 * 60 * 1000));
        const hourDiff = Math.floor((timestamp % (24 * 60 * 60 * 1000)) / (60 * 60 * 1000));
        const minuteDiff = Math.floor((timestamp % (60 * 60 * 1000)) / (60 * 1000));
        const secondDiff = Math.floor((timestamp % (60 * 1000)) / (1000));

        const dayString = String(dayDiff) + ' days ';
        const hourString = String(hourDiff) + ' hr ';
        const minuteString = String(minuteDiff) + ' min ';
        const secondString = String(secondDiff) + ' sec ';

        return (
            <React.Fragment>
                {dayString}
                {hourString}
                {minuteString}
                {secondString}
            </React.Fragment>
        )
    }

    render() {
        return (
            <Grid container direction="row" alignItems="flex-start" justify="flex-start" spacing={1}>
                {
                    this.props.photoList.length > 0 ?
                    this.props.photoList.map((photo, idx) => (
                        <Grid item xs={6} md={4} lg={2} key={idx}>
                            <Card key={this.formatTime(photo.timestamp - this.props.projectSelection.recipeStart)}>
                                <CardMedia component="img" image={photo.image} alt="Plant Photo" title="Placeholder Image" />
                                <CardHeader subheader={this.formatTime(photo.timestamp - this.props.projectSelection.recipeStart)} />
                            </Card>
                        </Grid>
                    ))
                    : null
                }
            </Grid>
        );
    }
}

export default PhotosPage;