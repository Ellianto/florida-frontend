import React from 'react';
import { Route, Switch } from 'react-router-dom';
import RecipeDetails from '../../components/Components_Web/RecipeDetails';
import RecipeSearch from '../../components/Components_Web/RecipeSearch';
import {searchRecipe} from '../../Logic/Logic_Web/RecipesLogic';

/**
 * !Props:
 * * Variables
 * 
 * * Methods
 * openSnackBar: HOC Methods to display a message via SnackBar
 * startLoading: HOC Methods to display LoadingOverlay
 * stopLoading : HOC Methods to hide LoadingOverlay
 * 
 * !States:
 * recipeList  : Array<JSObject>, represents search result triggered by RecipeSearch
 */

class RecipesPage extends React.Component{
    constructor(props){
        super(props);
        this.doSearch = this.doSearch.bind(this);

        this.state = {
            recipeList : [],
        }
    }

    async doSearch(query){
        this.props.startLoading();
        const recipeList = await searchRecipe(query);
        this.setState({
            recipeList: recipeList,
        });
        this.props.stopLoading();
    }

    render(){
        return(
            <Switch>
                <Route exact path='/recipes' render={props => <RecipeSearch {...props} openSnackBar={this.props.openSnackBar} recipeList={this.state.recipeList} doSearch={this.doSearch} handleRecipeCardClick={this.handleRecipeCardClick} />} />
                <Route exact path='/recipes/:recipeID' render={props => <RecipeDetails {...props} openSnackBar={this.props.openSnackBar} startLoading={this.props.startLoading} stopLoading={this.props.stopLoading}/>}/>
            </Switch>
        );
    }
}

export default RecipesPage;