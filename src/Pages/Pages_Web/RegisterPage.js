import { Button, Container, Paper, TextField, Typography, MenuItem } from '@material-ui/core';
import {Link} from 'react-router-dom';
import React from 'react';
import {getCities, getCountries, doRegister} from '../../Logic/Logic_Web/AuthLogic';

/**
 * !Props:
 * * Variables
 * style    : JSObject, passed from HOC to make the form centered 
 * 
 * * Methods
 * openSnackBar: HOC Method to display a message via SnackBar
 * startLoading: HOC Method to display LoadingOverlay
 * stopLoading : HOC Method to hide LoadingOverlay
 * clearUser   : HOC Method to do logical "log out"
 * 
 * !States:
 * uname        : String, represent input value for username
 * fname        : String, represent input value for first name
 * lname        : String, represent input value for last name
 * passwd       : String, represent input value for password
 * confPasswd   : String, represent input value for "confirm password"
 * country      : String, represent input value for country
 * city         : String, represent input value for city
 * matchPassword: Boolean, checks whether the confPasswd is equal to passwd
 * cities       : Array<JSObject>, represents the choices of cities available based on the selected country
 * countries    : Array<JSObject>, represents the choices of countries available
 */

 //TODO: Use inner variables instead of states
class RegisterPage extends React.Component {
    constructor(props) {
        super(props);

        this.handleSignUp = this.handleSignUp.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleCityChange = this.handleCityChange.bind(this);
        this.handleCountryChange = this.handleCountryChange.bind(this);
        this.handleMatchPassword = this.handleMatchPassword.bind(this);

        this.passwd = '';
        this.confPasswd = '';

        this.state = {
            uname : '',
            fname : '',
            lname : '',
            passwd : '',
            confPasswd: '',
            country: '',
            city  : '',
            matchPassword : true,
            cities: [],
            countries: [],
        }
    }

    async componentDidMount(){
        localStorage.removeItem('AuthToken');
        this.props.clearUser();

        const countries = await getCountries();

        if(countries.length === 0){
            this.props.openSnackBar("Something Went Wrong! Try again later!");
        }

        this.setState({
            countries: countries,
            country: '',
            city: '',
            cities: [],
        })
    }
    
    handleSignUp(){
        this.props.startLoading();
        let snackBarString = 'Make sure to re-type the correct password!';

        if(this.state.matchPassword){
            const countryName = this.state.country;
            const cityName = this.state.city;
            const username = this.state.uname;
            const password = this.state.passwd;
            const firstName = this.state.fname;
            const lastName = this.state.lname;

            if(doRegister(username, password, firstName, lastName, countryName, cityName)){
                snackBarString = 'Successfully Registered!';
                this.props.history.push('/login');
            } else {
                snackBarString = 'Failed to Register! Something wrong happened!';
            }
        }
        this.props.stopLoading();
        this.props.openSnackBar(snackBarString);
    }

    handleTextChange(e) {
        this.setState({
            [e.target.name] : e.target.value,
        })

        if(e.target.name === 'passwd' || e.target.name === 'confPasswd'){
            this.handleMatchPassword();
        }
    }

    async handleCountryChange(e) {
        this.props.startLoading();
        const countryName = e.target.value;

        function findCountry(country){return country.name === countryName}

        const countryID = this.state.countries.find(findCountry).id;

        const cities = await getCities(countryID);

        if(cities.length === 0){
            this.props.openSnackBar('Error Getting Cities List!');
        }

        this.setState({
            country: countryName,
            city: '',
            cities: cities,
        });
        this.props.stopLoading();
    }

    handleCityChange(e) {
        this.setState({
            city: e.target.value,
        });
    }

    handleMatchPassword() {
        this.setState((state,props) => ({
            matchPassword: state.passwd === state.confPasswd
        }));
    }

    render() {
        return (
            <div style={{ background: 'url(/img/home-banner-bg.png) no-repeat', backgroundSize: 'cover' , backgroundPosition: 'center center' }}> 
                <Container maxWidth="sm" style={{ paddingTop: '72px'}}>
                    <Paper elevation={6} style={{ padding: "24px"}}>
                        <Typography variant="h4" align="center"> Register User </Typography>
                        <TextField
                            id="first-name"
                            name="fname"
                            variant="filled"
                            label="First Name"
                            margin="normal"
                            type="text"
                            fullWidth={true}
                            onChange={this.handleTextChange}
                            required
                        />
                        <TextField
                            id="last-name"
                            name="lname"
                            variant="filled"
                            label="Last Name"
                            margin="normal"
                            type="text"
                            fullWidth={true}
                            onChange={this.handleTextChange}
                            required
                        />
                        <TextField
                            id="u-name"
                            name="uname"
                            variant="filled"
                            label="Username"
                            margin="normal"
                            type="text"
                            fullWidth={true}
                            onChange={this.handleTextChange}
                            required
                        />
                        <TextField
                            id="pass-wd"
                            name="passwd"
                            variant="filled"
                            label="Password"
                            margin="normal"
                            type="password"
                            fullWidth={true}
                            onChange={this.handleTextChange}
                            required
                        />
                        <TextField
                            error={!this.state.matchPassword}
                            id="conf-pass"
                            name="confPasswd"
                            variant="filled"
                            label="Confirm Password"
                            margin="normal"
                            type="password"
                            fullWidth={true}
                            onChange={this.handleTextChange}
                            required
                        />
                        {
                            this.state.countries.length === 0 ? null : 
                                <TextField select
                                    fullWidth
                                    value={this.state.country}
                                    onChange={this.handleCountryChange}
                                    variant='filled'
                                    label='Origin Country'
                                    margin='normal'
                                >
                                    {
                                        this.state.countries.map(country => <MenuItem key={country.id} value={country.name}> {country.name} </MenuItem>)
                                    }
                                </TextField>
                        }
                        {
                            this.state.cities.length === 0 ? null : 
                                <TextField select
                                    fullWidth
                                    value={this.state.city}
                                    onChange={this.handleCityChange}
                                    variant='filled'
                                    label='Origin City'
                                    margin='normal'
                                >
                                    { this.state.cities.map(city => <MenuItem key={city.id} value={city.name}> {city.name} </MenuItem>) }
                                </TextField>
                        }
                        <Button fullWidth={true} onClick={this.handleSignUp} variant="contained" size="large" color="primary" style={{ marginTop: '16px' }}> Register </Button>
                        <Link to='/' style={{ textDecoration: 'none' }}> <Button fullWidth={true} variant="contained" size="large" color="secondary" style={{ marginTop: '16px' }}> Cancel </Button> </Link>
                    </Paper>
                </Container>
            </div>
        );
    }
}

export default RegisterPage;