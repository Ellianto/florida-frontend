import React from 'react';
import {TextField, Button, Typography, Container, Paper} from '@material-ui/core';
import {Link} from 'react-router-dom';
import {doLogin} from '../../Logic/Logic_Web/AuthLogic';

/**
 * !Props
 * * Variables
 * style    : JSObject, passed from HOC to make the form centered 
 * 
 * * Methods
 * openSnackBar: HOC Method to display a message via SnackBar
 * startLoading: HOC Method to display LoadingOverlay
 * stopLoading : HOC Method to hide LoadingOverlay
 * clearUser   : HOC Method to do logical "log out"
 * setUser     : HOC Method to do logical "log in"
 */

class LoginPage extends React.Component{
    constructor(props){
        super(props);

        this.handleLogin = this.handleLogin.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);

        this.uname = '';
        this.passwd = '';
    }

    componentDidMount(){
        localStorage.removeItem('AuthToken');
        this.props.clearUser();
    }

    handleTextChange(e){
        if(e.target.name === 'uname'){
            this.uname = e.target.value;
        } else if(e.target.name === 'passwd'){
            this.passwd = e.target.value;
        }
    }

    async handleLogin(){
        this.props.startLoading();
        const loginSuccess = await doLogin(this.uname, this.passwd);

        if(loginSuccess){
            await this.props.setUser();
            this.props.history.push('/');
        } else {
            this.props.openSnackBar('Invalid Username & Password!');
        }

        this.props.stopLoading();
    }

    render(){
        return (
            <div style={{ background: 'url(/img/home-banner-bg.png) no-repeat', backgroundSize: 'cover' , backgroundPosition: 'center center' }}>             
                <Container maxWidth="sm" style={{paddingTop: '144px', height:'100vh'}}>
                    <Paper elevation={6} style={{padding:"24px"}}>
                        <Typography variant="h4" align="center"> Login </Typography>
                        <TextField
                            id="u-name"
                            name="uname"
                            variant="filled"
                            label="Username"
                            margin="normal"
                            fullWidth={true}
                            onChange={this.handleTextChange}
                            type="text"
                            required
                        />
                        <TextField 
                            id="pass-wd"
                            name="passwd"
                            variant="filled"
                            label="Password"
                            margin="normal"
                            fullWidth={true}
                            onChange={this.handleTextChange}
                            type="password"
                            required
                        />
                        <Button fullWidth={true} onClick={this.handleLogin} variant="contained" size="large" color="primary" style={{marginTop:'16px'}}> Login </Button>
                        <Link to='/' style={{textDecoration: 'none'}}> <Button fullWidth={true} variant="contained" size="large" color="secondary" style={{ marginTop: '16px' }}> Cancel </Button> </Link>
                    </Paper>
                </Container>
            </div>
        );
    }
}

export default LoginPage;